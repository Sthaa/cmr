"""

create role cmr;
alter role cmr with login;
alter role cmr createdb;
alter database c2db owner to cmr;
"""
import http
import os
import sys
from pathlib import Path
import subprocess
from urllib.request import urlopen

from ase.db import connect


home = Path.home()
dbfiles = home / 'data'
uwsgi = home / 'uwsgi'
static = uwsgi / 'static'
password = os.environ['CMR_PASSWORD']


def shell(cmd: str) -> None:
    print(cmd)
    subprocess.run(cmd, shell=True, check=True)


def psql(stmt: str) -> None:
    shell(f'psql -c "{stmt};" postgres')


def update(name: str) -> None:
    dbpath = dbfiles / f'{name}.db'
    if not dbpath.is_file():
        return

    old = connect(f'postgresql://ase:{password}@localhost:5432/{name}')
    new = connect(dbpath)

    nold = len(old)
    nnew = len(new)
    print(f'{nnew} rows in new database ({nold} in the old one)')
    if nnew < nold:
        return

    print(f'Inserting {nnew} rows:', end=' ', flush=True)
    with connect(f'postgresql://ase:{password}@localhost:5432/cmrtmp') as tmp:
        i = 0.0
        for row in new.select():
            kvp = row.get('key_value_pairs', {})
            tmp.write(row, data=row.get('data'), **kvp)
            i += 1
            if i >= nnew / 50:
                print(end='.', flush=True)
                i -= nnew / 50
    print('done')

    shell('cd ~/cmr; git pull')

    tarpath = dbfiles / f'static-{name}.tar.gz'
    if tarpath.is_file():
        for path in static.glob(f'{name}-*'):
            path.unlink()
        shell(f'cd {uwsgi}; tar -xf {tarpath}')

    try:
        shell('killall -v uwsgi')
    except subprocess.CalledProcessError:
        pass

    if (uwsgi / 'uwsgi.log').is_file():
        n = 1
        for logfile in uwsgi.glob('uwsgi.log.*'):
            if not logfile.name.endswith('gz'):
                n = max(n, int(logfile.name.rsplit('.', 1)[1]))
        shell(f'cd {uwsgi}; gzip uwsgi.log; mv uwsgi.log.gz uwsgi.log.{n}.gz ')

    psql(f'DROP DATABASE {name}')
    psql(f'ALTER DATABASE cmrtmp RENAME TO {name}')
    psql(f'CREATE DATABASE cmrtmp')

    conf = home / 'cmr' / 'cmrconf.py'
    shell(f'ASE_DB_APP_CONFIG={conf} '
          'uwsgi ~/cmr/uwsgi.ini --daemonize=$HOME/uwsgi/uwsgi.log')

    print('Testing page ... ', end='')
    for i in range(5):
        try:
            with urlopen('http://localhost:8000') as fd:
                fd.read()
            break
        except http.client.RemoteDisconnected:
            pass
    else:
        assert 0
    with urlopen(f'http://localhost:8000/{name}') as fd:
        txt = fd.read().decode()
    for line in txt.splitlines():
        if line.strip().startswith('Displaying rows'):
            nrows = int(line.split('<')[0].split()[-1])
            assert nrows == nnew, (nrows, nnew)
            break
    else:
        assert 0
    print('OK')


if __name__ == '__main__':
    update(sys.argv[1])
