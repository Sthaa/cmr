downloads = [
    ('dssc', ['dssc.db']),
    ('dcdft', ['dcdft.db', 'dcdft_gpaw_pw_paw09.db']),
    ('beef', ['molecules.db', 'solids.db']),
    ('mp_gllbsc', ['mp_gllbsc.db']),
    ('organometal', ['organometal.db']),
    ('cubic_perovskites', ['cubic_perovskites.db']),
    ('low_symmetry_perovskites', ['low_symmetry_perovskites.db']),
    ('c2db', ['c2db.db', 'workflow.png']),
    ('vdwh', ['chi-data.tar.gz', 'graphene-data.tar.gz']),
    ('tmfp06d', ['tmfp06d.db']),
    ('absorption_perovskites', ['absorption_perovskites.db']),
    ('funct_perovskites', ['funct_perovskites.db']),
    ('fcc111', ['fcc111.db']),
    ('compression', ['compression.db']),
    ('gbrv', ['gbrv.db']),
    ('g2', ['g2.db']),
    ('abx2', ['abx2.db']),
    ('solar', ['solar.db']),
    ('catapp', ['catapp.db', 'catappdata.csv']),
    ('adsorption', ['adsorption.db', 'surfaces.db']),
    ('abs3', ['abs3.db']),
    ('oqmd12', ['oqmd12.db']),
    ('pv_pec_oqmd', ['pv_pec_oqmd.db']),
    ('agau309', ['agau309.db']),
    ('a2bcx4', ['a2bcx4.db']),
    ('mixdim', ['mixdim.db']),
    ('c2dm', ['c2dm.db'])]

# Add pictures for the front-page:
downloads += [('images', [dir + '.png'
                          for dir, names in downloads])]

downloads.append(('surfaces', ['surfaces.db']))
