from __future__ import print_function
import csv
import os.path as op

try:
    from urllib.request import urlretrieve
except ImportError:
    from urllib import urlretrieve

from ase.utils.sphinx import create_png_files
from downloads import downloads

url = 'https://cmr.fysik.dtu.dk/_downloads/'


def download():
    """Download the big data files and front-page images for each project."""
    for dir, names in downloads:
        for name in names:
            path = op.join(dir, name)
            if not op.isfile(path):
                print('Downloading', path)
                if dir == 'images':
                    name = '../_images/' + name
                urlretrieve(url + name, path)


def create_key_description_tables():
    for dir, names in downloads:
        for name in names:
            if name.endswith('.db'):
                path = op.join(dir, 'custom.py')
                output = op.join(dir, 'keytable.csv')
                if op.isfile(path):
                    data = {}
                    with open(path) as i:
                        exec(i.read(), data)
                        if 'key_descriptions' not in data:
                            continue
                        kd = data['key_descriptions']

                    with open(output, 'w') as o:
                        writer = csv.writer(o)
                        writer.writerow(['key', 'description', 'unit'])
                        for key, val in sorted(kd.items()):
                            writer.writerow([key, val[1] or val[0], val[2]])


def setup(app):
    """Sphinx entry point."""
    download()
    create_key_description_tables()
    create_png_files()


if __name__ == '__main__':
    download()
