from ase.db import connect

db1 = connect('abs3.db')
with connect('abs3-new.db') as db2:
    for row in db1.select():
        kvp = row.key_value_pairs
        for key1, key2 in [('E_uncertanty_hull', 'E_uncertainty_hull'),
                           ('E_uncertanty_perAtom', 'E_uncertainty_per_atom'),
                           ('E_relative_perAtom', 'E_relative_per_atom')]:
            if key1 in row:
                kvp[key2] = kvp.pop(key1)
        db2.write(row, data=row.data, **kvp)
