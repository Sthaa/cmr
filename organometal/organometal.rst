.. _organometal:

Organometal Halide Perovskites
==============================

.. container:: article

    Ivano E. Castelli, Juan María García-Lastra, Kristian S. Thygesen,
    and Karsten W. Jacobsen

    `Bandgap Calculations and Trends of Organometal Halide Perovskites`__

    APL Materials, July 21, 2014

    __ http://dx.doi.org/10.1063/1.4893495


.. contents::

* :download:`Download raw data <organometal.db>`
* `Browse data <http://cmrdb.fysik.dtu.dk/organometal>`_


Key-value pairs
---------------

.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1

Example
-------

.. include:: example.py
   :code: python

.. image:: example.svg
