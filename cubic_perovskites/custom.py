title = 'Perovskite water-splitting'

key_descriptions = {
    'A_ion': ('A-ion', 'A-ion in the cubic perovskite', ''),
    'B_ion': ('B-ion', 'B-ion in the cubic perovskite', ''),
    'anion': ('Anion', 'Anion combination in the perovskite', ''),
    'gllbsc_dir_gap': ('GLLB dir. gap', 'Direct bandgap calculated with GLLB-SC', 'eV'),
    'gllbsc_ind_gap': ('GLLB ind. gap', 'Indirect bandgap calculated with GLLB-SC', 'eV'),
    'heat_of_formation_all': ('HOF', 'Heat of formation calculated with respect to all the materials in the pool of references', 'eV'),
    'combination': ('Type', 'General formula', ''),
    'CB_dir': ('Dir. Cond.', 'Direct position of the conduction band edge', ''),
    'CB_ind': ('Ind. Cond', 'Indirect position of the conduction band edge', ''),
    'VB_dir': ('Dir. Val.', 'Direct position of the valence band edge', ''),
    'VB_ind': ('Ind. Val.', 'Indirect position of the valence band edge', ''),
    'reference': ('Reference', 'standard (used to calculate the standard heat of formation) or pool (reference included in the calculation of the convex hull)', '')}

default_columns = ['id', 'age', 'formula', 'energy', 'pbc', 'volume', 'charge', 'gllbsc_dir_gap', 'gllbsc_ind_gap', 'heat_of_formation_all']
