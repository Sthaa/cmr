"""

::

    export ASE_DB_APP_CONFIG=~/cmr/cmrconf.py
    uwsgi uwsgi.ini
"""

import os
import subprocess
from pathlib import Path

names = [
    'dssc',
    'mp_gllbsc',
    'organometal',
    'cubic_perovskites',
    'low_symmetry_perovskites',
    'c2db',
    'tmfp06d',
    'absorption_perovskites',
    'funct_perovskites',
    'fcc111',
    'compression',
    'g2',
    'catapp',
    'abx2',
    'solar',
    'agau309',
    'adsorption',
    'surfaces',
    'abs3',
    'oqmd12',
    'pv_pec_oqmd',
    'a2bcx4',
    'mixdim']

pw = os.environ.get('CMR_PASSWORD', 'ase')

home = Path.home()

ASE_DB_TMPDIR = home / 'uwsgi' / 'static'
ASE_DB_DOWNLOAD = False
ASE_DB_HOMEPAGE = 'https://cmr.fysik.dtu.dk/'
ASE_DB_FOOTER = """\
This work is licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br />
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
<img alt="Creative Commons License" style="border-width:0"
src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>
"""

ASE_DB_NAMES = []
for db in names:
    url = f'postgresql://ase:{pw}@localhost:5432/{db}'
    py = home / 'cmr' / db / 'custom.py'
    ASE_DB_NAMES.extend([url, py])


if 0:
    for db in names:
        print(f'create database {db};')

if 0:
    for db in names:
        if db in []:  # ['mixdim', 'oqmd12', 'c2db', 'abs3']:
            continue
        subprocess.run(
            f'ase db {db}/{db}.db -i postgresql://ase:{pw}@localhost:5432/{db}',
            shell=True)
