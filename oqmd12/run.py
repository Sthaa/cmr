import sys
from ase.build import niggli_reduce
import traceback
from ase.db import connect
from gpaw import GPAW, PW, FermiDirac, Davidson
import numpy as np

if len(sys.argv) == 2:
    pbeu = 'u' in sys.argv[1]
    mag = 'm' in sys.argv[1]
else:
    pbeu = False
    mag = False

kptdensity = 6.0
width = 0.05
ecut = 800
xc = 'PBE'

# U values are from AFLOW.  Based on some papers mentioned here:
#
#     https://pubs.acs.org/doi/10.1021/co200012w
u_atoms = {'Ti': 4.4,
           'V': 2.7,
           'Cr': 3.5,
           'Mn': 4.0,
           'Fe': 4.6,
           'Co': 5.0,
           'Ni': 5.1,
           'Cu': 4.0,
           'Zn': 7.5,
           'Nb': 2.1,
           'Mo': 2.4,
           'Tc': 2.7,
           'Ru': 3.0,
           'Rh': 3.3,
           'Pd': 3.6,
           'Cd': 2.1,
           'Ta': 2.0,
           'W': 2.2,
           'Re': 2.4,
           'Os': 2.6,
           'Ir': 2.8,
           'Pt': 3.0}

mag_elements = {'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn',
                'Y', 'Zr', 'Nb', 'Mo', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 'In',
                'Hf', 'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', 'Tl'}

db = connect('../gpaw-in.db')
out = connect('gpaw.db')

for row in db.select():
    if pbeu:
        setups = {symbol: ':d,{:.1f}'.format(u)
                  for symbol, u in u_atoms.items()
                  if symbol in row.symbols}
        if len(setups) == 0:
            continue
    else:
        setups = 'paw'

    if mag:
        for symbol in row.symbols:
            if symbol in mag_elements:
                break
        else:
            continue

    atoms = row.toatoms()

    if len(atoms) > 24:
        continue

    id = out.reserve(name=row.formula)
    if id is None:
        continue
    try:
        if mag:
            atoms.set_initial_magnetic_moments(np.ones(len(atoms)))

        def calculate(niggli=False):
            atoms.calc = GPAW(
                mode=PW(ecut),
                eigensolver=Davidson(3),
                xc=xc,
                setups=setups,
                basis='dzp',
                # basis='dzp',
                # parallel={'band': 1},
                kpts={'density': kptdensity},
                occupations=FermiDirac(width=width),
                symmetry={'do_not_symmetrize_the_density': True},
                txt=row.formula + '.txt')
            atoms.get_potential_energy()
            atoms.get_forces()
            atoms.get_stress()
            atoms.calc.write(row.formula + '.gpw')
            out.write(atoms, name=row.formula, niggli=niggli)

        try:
            calculate()
        except ValueError:
            niggli_reduce(atoms)
            calculate(True)

    except Exception:
        with open(row.formula + '.err', 'w') as f:
            traceback.print_exc(file=f)
        out.write(atoms, name=row.formula, failed=True)
    del out[id]
