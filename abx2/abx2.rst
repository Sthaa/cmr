.. _abx2:

Database of ABX2 materials
==========================

.. container:: article

    Mohnish Pandey, Korina Kuhar, and Karsten W. Jacobsen

    `II–IV–V2 and III–III–V2 Polytypes as Light
    Absorbers for Single Junction and Tandem Photovoltaic Devices`__

    J. Phys. Chem. C, 2017, 121 (33), pp 17780–17786

    __ http://pubs.acs.org/doi/abs/10.1021/acs.jpcc.7b07437

.. contents::


The data
--------

* Download database: :download:`abx2.db`
* `Browse data <http://cmrdb.fysik.dtu.dk/abx2>`_


Key-value pairs
---------------


.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1


Example
-------

mBEEF energies and the corresponding uncertainties for three phases:

.. image:: enthalpy.png
.. literalinclude:: enthalpy.py
