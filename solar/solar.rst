.. _solar:

Database of Organic donor-acceptor molecules
============================================

.. contents::


The data
--------

* Download database: :download:`solar.db`
* `Browse data <http://cmrdb.fysik.dtu.dk/solar>`_


Key-value pairs
---------------


.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1

