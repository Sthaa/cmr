# creates: formation_energies.svg
import numpy as np
import matplotlib.pyplot as plt
from ase.db import connect

db = connect('agau309.db')
num_au = []
energies = []
for row in db.select():
    count = row.count_atoms().get('Au', 0)
    num_au.append(count)
    energies.append(row.energy)
    natoms = row.natoms

num_au = np.array(num_au)
energies = np.array(energies)
E0 = energies[0]
E1 = energies[-1]
fracs = num_au.astype(np.double) / natoms
Ef = (energies - E0) + (E0 - E1) * fracs

plt.plot(num_au, 1000 * Ef / natoms)
plt.ylabel('Energy of formation (meV / atom)')
plt.xlabel('Number of gold atoms')
plt.savefig('formation_energies.svg', bbox_inches='tight')
plt.show()
