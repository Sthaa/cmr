title = 'Low symmetry perovskites'

key_descriptions = {
    'gllbsc_dir_gap': ('GLLB dir. gap', 'Direct bandgap calculated with GLLB-SC', 'eV'),
    'gllbsc_ind_gap': ('GLLB ind. gap', 'Indirect bandgap calculated with GLLB-SC', 'eV'),
    'gllbsc_disc': ('GLLB der. disc.', 'Derivative discontinuity calculated with GLLB-SC', 'eV'),
    'phase': ('Phase', 'double ruddlesden-popper or dion-jacobsen', ''),
    'ruddlesden_popper': ('RP Phase', 'Ruddlesden popper phase A2BO4 A3B2O7 or A2BO3N', ''),
    'dion_jacobsen': ('DJ Phase', '1: A=Na Mg K Rb or 2: A=Rb Sr Cs Ba', '')}

default_columns = ['id', 'age', 'formula', 'energy', 'pbc', 'volume', 'charge', 'gllbsc_dir_gap', 'gllbsc_ind_gap', 'phase']
