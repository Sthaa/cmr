from ase.db import connect
db1 = connect('A2BCX4.db')
db2 = connect('a2bcx4.db')
for row in db1.select():
    kvp = row.key_value_pairs
    kvp['E_relative_per_atom'] = kvp.pop('E_relative_perAtom')
    kvp['E_uncertainty_per_atom'] = kvp.pop('E_uncertainty_perAtom')
    db2.write(row, data=row.data, **kvp)
