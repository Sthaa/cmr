import os
from ase.db import connect
import interval_analysis
import rank_determination
import topological_scaling


def run():
    folder = '/media/pete/f0678ac5-b006-4d9c-959c-55c24342277a/pete_old/Desktop/_phd_data/dimensionality/'
    path = os.path.join(folder, 'icsd_all.db')
    db = connect(path)
    rows = [row for row in db.select('rfactor>=0') if row.natoms <= 20]
    rows = rows[:100]
    data = [(db.get_atoms(id=row.id), row.dbid, row.source) for row in rows]

    for atoms, dbid, source in data:
        if 1:
            method = rank_determination.RDA
        else:
            method = topological_scaling.TSA

        intervals = interval_analysis.identify_components(atoms, method)
        (score, a, b, h, components) = intervals[0]
        print(dbid, sum([e[0] for e in intervals]))
        # print(dbid, h, score, a, b)


run()
