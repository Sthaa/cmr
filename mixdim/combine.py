

if __name__ == '__main__':
    import ase.db

    c1 = ase.db.connect('cod.db')
    c2 = ase.db.connect('icsd.db')
    rows1 = [r for r in c1.select()]
    rows2 = [r for r in c2.select()]
    with ase.db.connect('cod-icsd.db') as c:
        for r in rows1:
            kvp = r.key_value_pairs
            kvp['source'] = 'cod'
            c.write(r.toatoms(), key_value_pairs=kvp, data=r.data)
        for r in rows2:
            kvp = r.key_value_pairs
            kvp['source'] = 'icsd'
            c.write(r.toatoms(), key_value_pairs=kvp, data=r.data)
