# creates: mixdims.png
from ase.db import connect
import matplotlib.pyplot as plt
import numpy as np
from typing import List, Iterable
from ase.db.row import AtomsRow

keys = ['s_0',
        's_1',
        's_2',
        's_3',
        's_01',
        's_02',
        's_03',
        's_12',
        's_13',
        's_23',
        's_012',
        's_013',
        's_023',
        's_123',
        's_0123']


def most_stable(rows: Iterable[AtomsRow],
                keys: List[str],
                scorekey: str) -> List[int]:
    """indices of materials for which the scorekey has the largest value
    of all possbile scores.
    """
    indices = []
    for i, r in enumerate(rows):
        diff = (np.array([r.get(k) for k in keys if k != scorekey]) -
                r.get(scorekey))
        if np.all(diff < 0):
            indices.append(i)
    return indices


def print_count(rows: Iterable[AtomsRow],
                keys: List[str]) -> None:
    dimentionality_counts = [len(most_stable(rows, keys, scorekey=k))
                             for k in keys]
    for k, n in zip(keys, dimentionality_counts):
        print('number of materials'
              'with dimentionality {}: {}'.format(k.split('_')[-1], n))


def makefig(rows: List[AtomsRow],
            keys: List[str]) -> None:
    fig, ax = plt.subplots()

    for scorekey in ['s_2', 's_3']:
        indices = most_stable(rows, keys, scorekey=scorekey)
        myscores = [rows[i].get('s_2') for i in indices]
        label = '{}D material'.format(scorekey.split('_')[-1])
        ax.hist(myscores, bins=40, alpha=0.6, label=label)

    ax.set_ylim(0, 250)
    ax.set_xlim(0, 1)
    ax.set_xlabel('2D score')
    ax.set_ylabel('Frequency')
    plt.legend()
    plt.tight_layout()
    plt.savefig('mixdims.png')
    plt.close()


def run_example():
    c = connect('mixdim.db')
    rows = [r for r in c.select()]
    print_count(rows, keys=keys)
    makefig(rows, keys=keys)


run_example()
