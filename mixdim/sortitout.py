"""script for sorting data and striping positions and cell data
"""
from typing import List
from ase.db import connect
from ase.db.row import AtomsRow
from mixdim import most_stable


def sortit(rows: List[AtomsRow], keys: List[str]):
    dim_classes = [most_stable(rows, keys, scorekey=k) for k in keys]

    indices = []
    for k, cindices in zip(keys, dim_classes):
        js = zip(range(len(cindices)), [rows[i].get(k) for i in cindices])
        indices.extend([cindices[i]
                        for i, _ in sorted(js, key=lambda x: x[1],
                                           reverse=True)])
    return indices


def sortit2(rows: List[AtomsRow], keys: List[str]):
    indices = range(len(rows))
    for k in keys:
        stuff = sorted([(i, rows[i].get(k)) for i in indices],
                       key=lambda x: x[1], reverse=True)
        indices = [i for i, _ in stuff]
    return indices


def write_db(name: str,
             rows: List[AtomsRow],
             indices: List[int],
             strip_positions: bool = False):
    """write only the indices of rows listed in indices to file called name.
    the keyword strip indicates whether positions and the unitcell should be
    stripped off.
    """
    with connect(name) as db:
        for i in indices:
            r = rows[i]
            a = r.toatoms()
            if strip_positions:
                a.positions[:] = 0
                a.cell = (1, 1, 1)
            db.write(a, key_value_pairs=r.key_value_pairs, data=r.data)


if __name__ == '__main__':
    # from mixdim import keys
    c = connect('./cod_combined.db')
    rows = [r for r in c.select()]
    # indices = sortit(rows, keys)a
    keys = ['numc_{}D'.format(d) for d in range(4)]
    indices = sortit2(rows, keys=keys)
    write_db(name='cod.db',
             rows=rows,
             indices=indices,
             strip_positions=False)
