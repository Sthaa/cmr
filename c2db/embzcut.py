# Creates: em_bs_spin={}_band={}_bt={}_soc={}.gpw
import os.path as op

import numpy as np

from ase.units import Hartree, Bohr
from ase.dft.kpoints import kpoint_convert

from gpaw import GPAW

from c2db.em import _readmass


def bzcut(ke_v, mass, kd_v, sbbtsoc=(None, ) * 4,
          erange=500e-3, npoints=91, gpw='densk.gpw'):
    """
    Band structure allong effective mass directions, the path length in
    k-space is adjusted to reach erange at the ends
    Parameters:
         ke_v: (3,)-shape ndarray
            center for the kpt-line cut in cartesian (1 / Bohr)
        mass: float
            effective mass in units of m_e
        kd_v: (3,)-shape ndarray
            direction for the kpoint line in cartesian
        sbbtsoc: (4,)-shape tuple
            it should contain (spin: int, band: int, bandtype: str, soc: bool)
        erange: float
            used to estimate range of k-points
        npoints: int
            number of k-points on the path
        gpw: str
            gpw filename from where the atoms and calc parameters are taken

    """
    calc = GPAW(gpw, txt=None)
    cell_cv = calc.atoms.get_cell()
    kmax = np.sqrt(2 * abs(mass) * erange / Hartree)  # 1 / Bohr
    k_kv = (np.linspace(-1, 1, npoints) * kmax * kd_v.reshape(3, 1)).T
    k_kv += ke_v  # add extremum
    k_kv /= Bohr  # now in 1 / Angstrom (1 / Bohr = 1 / (0.527 Angstrom))
    k_kc = kpoint_convert(cell_cv=cell_cv, ckpts_kv=k_kv)
    name = 'em_bs_spin={}_band={}_bt={}_soc={}'.format(*sbbtsoc)
    if op.isfile(name + '.gpw'):
        return
    calc.set(kpts=k_kc, txt=name + '.txt', symmetry='off')
    calc.get_potential_energy()
    calc.write(name + '.gpw')


def get_bzcut_names(soc, bt):
    d = _readmass(soc=soc, bt=bt)
    names = []
    for k in d.keys():
        mass_u = d[k]['mass_u']
        for i, m in enumerate(mass_u):
            bt2 = bt + '_m({}){:.3e}'.format(i, m)
            vals = [k.spin, k.band, bt2, soc]
            name = 'em_bs_spin={}_band={}_bt={}_soc={}.gpw'.format(*vals)
            names.append(name)
    return names


def runcuts(socs=(True, False), bandtypes=('vb', 'cb')):
    for soc in socs:
        for bt in bandtypes:
            try:
                mass = _readmass(soc=soc, bt=bt)
                for k in mass['indices']:
                    v = mass[k]
                    m_u = v['mass_u']
                    ke_v = v['ke_v']  # 1 / Bohr
                    kd_vu = v['eigenvectors_vu']
                    for i, (m, kd_v) in enumerate(zip(m_u, kd_vu.T)):
                        sbbtsoc = (k.spin, k.band,
                                   bt + '_m({}){:.3e}'.format(i, m), soc)
                        kd_v = np.append(kd_v, 0.0)
                        kd_v /= np.linalg.norm(kd_v)
                        bzcut(ke_v=ke_v, mass=m, kd_v=kd_v, sbbtsoc=sbbtsoc)
            except Exception as x:
                print(x)


if __name__ == '__main__':
    runcuts()
