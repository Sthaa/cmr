.. _c2db:

==========================================
Computational 2D Materials Database (C2DB)
==========================================

If you are using data from this database in your research, please cite the
following paper:

.. container:: article

    `The Computational 2D Materials Database: High-Throughput Modeling
    and Discovery of Atomically Thin Crystals`__

    Sten Haastrup, Mikkel Strange, Mohnish Pandey, Thorsten Deilmann,
    Per S. Schmidt, Nicki F. Hinsche, Morten N. Gjerding, Daniele Torelli,
    Peter M. Larsen, Anders C. Riis-Jensen, Jakob Gath, Karsten W. Jacobsen,
    Jens Jørgen Mortensen, Thomas Olsen, Kristian S. Thygesen

    2D Materials 5, 042002 (2018)

    __ http://iopscience.iop.org/article/10.1088/2053-1583/aacfc1

The data can be downloaded or browsed online:

* `Browse data <https://cmrdb.fysik.dtu.dk/c2db>`_
* Download data: :download:`c2db.db`


.. contents::

Brief description
=================

The database contains structural, thermodynamic, elastic, electronic,
magnetic, and optical properties of around 2000 two-dimensional materials
distributed over more than 30 different crystal structures. The properties
are calculated by DFT and many-body perturbation theory (G0W0 and the Bethe-
Salpeter Equation for around 200 materials) using the GPAW code and a semi-
automated ASE based workflow. The workflow and a table with the numerical
settings employed for the calculation of the different properties is provided
below.

.. image:: workflow.png


Overview of methods and parameters used
=======================================

If a parameter is not specified at a given step, its value equals that
of the last step where it was specified:

.. list-table::
  :widths: 10 25
  :header-rows: 1

  * - Workflow step(s)
    - Parameters

  * - Structure and energetics(*) (1-4)
    - vacuum = 15 Å; `k`-point density = 6.0/Å\ `^{-1}`;
      Fermi smearing = 0.05 eV; PW cutoff = 800 eV;
      xc functional = PBE; maximum force = 0.01 eV/Å;
      maximum stress = 0.002 eV/Å\ `^3`; phonon displacement = 0.01Å

  * - Elastic constants (5)
    - `k`-point density = `12.0/\mathrm{Å}^{-1}`; strain = `\pm`\ 1%

  * - Magnetic anisotropy (6)
    - `k`-point density = `20.0/\mathrm{Å}^{-1}`;
      spin-orbit coupling = True

  * - PBE electronic properties (7-10 and 12)
    - `k`-point density = `12.0/\mathrm{Å}^{-1}`
      (`36.0/\mathrm{Å}^{-1}` for DOS)

  * - Effective masses (11)
    - `k`-point density = `45.0/\mathrm{Å}^{-1}`; finite difference

  * - Deformation potential (13)
    - `k`-point density = 12.0/Å\ `^{-1}`; strain = `\pm`\ 1%

  * - Plasma frequency (14)
    - `k`-point density = 20.0/Å\ `^{-1}`; tetrahedral interpolation

  * - HSE band structure (8-12)
    - HSE06\ @PBE; `k`-point density = 12.0/Å\ `^{-1}`

  * - `G_0W_0` band structure (8, 9)
    - `G_0W_0`\ @PBE; `k`-point density = `5.0/\mathrm{Å}^{-1}`;
      PW cutoff = `\infty` (extrapolated from 170, 185 and 200 eV);
      full frequency integration;
      analytical treatment of `W({q})` for small `q`;
      truncated Coulomb interaction

  * - RPA polarisability (15)
    - RPA\ @PBE; `k`-point density = `20.0/\mathrm{Å}^{-1}`;
      PW cutoff = 50 eV; truncated Coulomb interaction;
      tetrahedral interpolation

  * - BSE absorbance (16)
    - BSE\ @PBE with `G_0W_0` scissors operator;
      `k`-point density = `20.0/\mathrm{Å}^{-1}`;
      PW cutoff = 50 eV; truncated Coulomb interaction;
      at least 4 occupied and 4 empty bands

(*) *For the cases with convergence issues, we set a \(k\)-point density of
9.0 and a smearing of 0.02 eV*.


Versions
========


.. list-table::
    :widths: 3 1 10

    * - Version
      - rows
      - comment
    * - 2018-06-01
      - 1888
      - Initial release
    * - 2018-08-01
      - 2391
      - Two new prototypes added
    * - 2018-09-25
      - 3084
      - New prototypes
    * - 2018-12-10
      - 3331
      - Some BSE spectra recalculated due to small bug affecting
        absorption strength of materials with large spin-orbit couplings


Key-value pairs
===============

.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1


Example
=======

The following python script shows how to plot the positions of the VBM and CBM.

.. literalinclude:: plot_band_alignment.py

This produces the figure

.. image:: band-alignment.png


Tools for creating the "Computational 2D materials" database
============================================================

Requirements
------------

* Python_ 3.5-
* ASE_ 3.16.2
* GPAW_ 1.4.0


Installation
------------

Clone the code from https://gitlab.com/camd/cmr and tell Python where to find
it::

    $ cd ~
    $ git clone git@gitlab.com:camd/cmr
    $ export PYTHONPATH=~/cmr:$PYTHONPATH


Workflow
--------

The workflow for each material is described in the :download:`submit.py`
file:

.. literalinclude:: submit.py

As an example, let's do MoS\ `_2` in the ``CdI2`` prototype (T-phase).
Create a main folder and a subfolder::

    $ mkdir TMD
    $ cd TMD
    $ mkdir MoS2-CdI2

Create a ``start.traj`` file containing something close to the groundstate
structure::

    $ python3
    >>> from ase.build import mx2
    >>> a = mx2('MoS2', '1T', a=3.20, thickness=3.17, vacuum=7.5)
    >>> a.write('MoS2-CdI2/start.traj')

Run the :download:`relax <relax.py>` step::

    $ mq submit c2db.relax -R 8x12h MoS2-CdI2/

or::

    $ mq workflow ~/cmr/c2db/submit.py MoS2-CdI2/

.. note::

    The `mq` tool is described here_

.. _here: https://myqueue.readthedocs.io/

Once the :download:`relax <relax.py>` task has finished, one or more
folders will appear (``nm/``, ``fm/``, ``afm/``) and you can run the whole
workflow::

    $ mq workflow ~/cmr/c2db/submit.py MoS2-CdI2/*m/

When everything is ready you can collect the results in a database file and
inspect the results in a web-browser::

    $ python3 -m c2db.collect -r oqmd12.db MoS2-CdI2/*m/ > collect.out
    $ ase db c2db.db -M ~/cmr/c2db/custom.py -w

where ``oqmd12.db`` is the database of 1- and 2-component reference systems.


Dependencies
------------

List of tasks, dependencies and files created (teporary files are in
parenthesis):

.. csv-table::
    :file: dependencies.csv
    :header: task, dependency, created files
    :widths: 1, 1, 3


.. _Python: http://www.python.org/
.. _ASE: http://wiki.fysik.dtu.dk/ase/
.. _GPAW: http://wiki.fysik.dtu.dk/gpaw/
