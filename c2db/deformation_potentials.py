import numpy as np
from ase.parallel import rank
from c2db.strains import collect_deformation_potentials, collect_mobilities


def run_deformation(out_archive='strain_quantities.npz', ionic_relax=True):
    try:
        data = dict(np.load(out_archive))
    except IOError:
        data = {}
        print('Error loading data')

    changed = False
    if 'deformation_potentials' not in data:
        data = collect_deformation_potentials(data=data,
                                              ionic_relax=ionic_relax)
        changed = True
    if 'electron_mobilities' not in data:
        data = collect_mobilities(data=data)
        changed = True
    if rank == 0 and changed:
        np.savez(out_archive, **data)
    return data


if __name__ == '__main__':
    run_deformation()
