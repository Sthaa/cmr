# Creates: phonon.*.pckl

import pickle
from pathlib import Path

import numpy as np
import ase.units as units
import ase.io.ulm as ulm
from ase.parallel import world
from ase.io import read
from ase.phonons import Phonons
from gpaw import GPAW


def phonons(N=2):
    state = Path().cwd().parts[-1]

    # Remove empty files:
    if world.rank == 0:
        for f in Path().glob('phonon.*.pckl'):
            if f.stat().st_size == 0:
                f.unlink()
    world.barrier()

    params = {}
    name = '../relax-{}.traj'.format(state)
    u = ulm.open(name)
    params.update(u[-1].calculator.parameters)
    u.close()

    # Set essential parameters for phonons
    params['symmetry'] = {'point_group': False}

    # Make sure to converge forces! Can be important
    if 'convergence' in params:
        params['convergence']['forces'] = 1e-6
    else:
        params['convergence'] = {'forces': 1e-6}
    
    oldcalc = GPAW('gs.gpw', txt=None)
    N_c = oldcalc.wfs.gd.N_c * [N, N, 1]
    params['gpts'] = N_c
    slab = read(name)

    name = 'phonons-N={}'.format(N)
    fd = open(name + '.txt', 'a')
    calc = GPAW(txt=fd, **params)

    # Set initial magnetic moments
    if state[:2] == 'fm' or state[:3] == 'afm':
        gsold = GPAW('gs.gpw', txt=None)
        magmoms_m = gsold.get_magnetic_moments()
        slab.set_initial_magnetic_moments(magmoms_m)
        
    p = Phonons(slab, calc, supercell=(N, N, 1),
                name=name)
    p.run()


def analyse(atoms, name='phonon', delta=0.01, D=3):
    N = len(atoms)
    C = np.empty((2, 2, N, D, 2, 2, N, D))
    for a in range(N):
        for i, v in enumerate('xyz'[:D]):
            forces = []
            for sign in '-+':
                filename = '{}.{}{}{}.pckl'.format(name, a, v, sign)
                with open(filename, 'rb') as fd:
                    f = pickle.load(fd)[:, :D]
                    f[a] -= f.sum(axis=0)
                    f.shape = (2, 2, N, D)
                    forces.append(f)
            C[0, 0, a, i] = (forces[0] - forces[1]) / (2 * delta)

    C[0, 1] = C[0, 0, :, :, :, ::-1]
    C[1, 0] = C[0, 0, :, :, ::-1]
    C[1, 1] = C[0, 0, :, :, ::-1, ::-1]

    C.shape = (4 * D * N, 4 * D * N)
    C += C.T.copy()
    C *= 0.5

    # Mingo correction.
    #
    # See:
    #
    #    Phonon transmission through defects in carbon nanotubes
    #    from first principles
    #
    #    N. Mingo, D. A. Stewart, D. A. Broido, and D. Srivastava
    #    Phys. Rev. B 77, 033418 – Published 30 January 2008
    #    http://dx.doi.org/10.1103/PhysRevB.77.033418

    R_in = np.zeros((4 * D * N, D))
    for n in range(D):
        R_in[n::D, n] = 1.0
    a_in = -np.dot(C, R_in)
    B_inin = np.zeros((4 * D * N, D, 4 * D * N, D))
    for i in range(4 * D * N):
        B_inin[i, :, i] = np.dot(R_in.T, C[i, :, np.newaxis]**2 * R_in) / 4
        for j in range(4 * D * N):
            B_inin[i, :, j] += np.outer(R_in[i], R_in[j]).T * C[i, j]**2 / 4
    # L_in = np.linalg.solve(B_inin.reshape((36 * N, 36 * N)),
    #                        a_in.reshape((36 * N,))).reshape((12 * N, D))
    L_in = np.dot(np.linalg.pinv(B_inin.reshape((4 * D**2 * N, 4 * D**2 * N))),
                  a_in.reshape((4 * D**2 * N,))).reshape((4 * D * N, D))
    D_ii = C**2 * (np.dot(L_in, R_in.T) + np.dot(L_in, R_in.T).T) / 4
    # print('Small number:', (D_ii**2 / C**2).max())
    C += D_ii

    # Conversion factor: sqrt(eV / Ang^2 / amu) -> eV
    s = units._hbar * 1e10 / (units._e * units._amu)**0.5
    M = np.repeat(atoms.get_masses(), D)
    invM = np.outer(M, M)**-0.5
    eigs = []
    freqs = []
    C.shape = (2, 2, N * D, 2, 2, N * D)
    for q in [(0, 0), (0, 1), (1, 0), (1, 1)]:
        K = np.zeros((N * D, N * D))
        for c1 in range(2):
            for c2 in range(2):
                K += (-1)**np.dot(q, (c1, c2)) * C[0, 0, :, c1, c2]
        e = np.linalg.eigvalsh(K)
        f2 = np.linalg.eigvalsh(invM * K)
        f = abs(f2)**0.5 * np.sign(f2) * s
        eigs.append(e)
        freqs.append(f)

    return np.array(eigs), np.array(freqs)


def main(args=None):
    import argparse
    desc = 'Calculate phonons'
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-N', help='Super-cell size', type=int, default=2)
    args = parser.parse_args(args)
    phonons(N=args.N)


if __name__ == '__main__':
    main()
