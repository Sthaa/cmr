from __future__ import print_function
import numpy as np
from collections import defaultdict
from io import StringIO
import spglib
from ase import Atoms
from ase.db import connect

import bulk_enumerator as be
from c2db.utils import get_reduced_formula


def get_symmetry_id(atoms, symprec):
    cell, spos, numbers = spglib.standardize_cell(atoms,
                                                  symprec=symprec)
    standard_atoms = Atoms(numbers=numbers, cell=cell, scaled_positions=spos,
                           pbc=[1, 1, 0])
    poscar_buffer = StringIO()
    standard_atoms.write(poscar_buffer, format='vasp')
    b = be.bulk.BULK()
    b.set_structure_from_file(poscar_buffer.getvalue())
    poscar_buffer.close()
    name = b.get_name()
    del b
    return name


prototype_labels = {'A_2_c_191': 'C',
                    'AB_2_d_d_164': 'CH',
                    'ABC2_1_a_b_ab_156': 'CH2Si',
                    'AB_1_a_b_156': 'GeSe',
                    'AB_1_a_c_187': 'BN',
                    'AB_2_g_h_187': 'GaS',
                    'AB_2_c_d_164': 'GaSe',
                    'AB_2_a_c_129': 'FeSe',
                    'AB_2_a_a_31': 'SnS',
                    'AB_1_a_c_123': 'PbSe',
                    'AB_2_c_c_129': 'PbS',
                    'AB_2_ae_m_10': 'AuSe',
                    'ABC_1_a_a_b_156': 'MoSSe',
                    'ABC_1_a_b_c_156': 'BiTeI',
                    'ABC_2_a_a_a_31': 'HfBrS',
                    'AB2_4_i2_i4_2': 'ReS2',
                    'AB2_1_a_g_115': 'GeS2',
                    'ABC_2_a_a_b_59': 'FeOCl',
                    'AB2_1_a_h_187': 'MoS2',
                    'AB2_1_a_d_164': 'CdI2',
                    'AB2_2_e_e2_11': 'WTe2',
                    'AB3_2_c_l_191': 'C3N',
                    'AB3_2_c_k_162': 'BiI3',
                    'AB3_2_c_i_191': 'AgBr3',
                    'AB3_2_a_be_59': 'TiS3',
                    'AB3_2_c_i_189': 'TiCl3',
                    'AB2C2_1_a_d_d_164': 'Ti2CO2',
                    'AB2C2D2_1_a_d_d_d_164': 'Ti2CH2O2',
                    'A2B3_1_g_ci_187': 'Ti3C2',
                    'A2B2C3_1_g_h_ai_187': 'Ti3C2O2',
                    'A2B2C2D3_1_g_g_h_ai_187': 'Ti3C2H2O2',
                    'A3B4_1_ad_cd_164': 'Ti4C3',
                    'A2B3C4_1_d_ad_cd_164': 'Ti4C3O2',
                    'A2B2C3D4_1_d_d_ad_cd_164': 'Ti4C3H2O2',
                    'Perovskite': 'PbA2I4',
                    'Undefined': 'Undefined'}

exceptions = {'ABC4_2_a_c_c2d_28': 'ABC4_1_a_c_gh_25',
              'AB3C8_1_a_bc_a2b2c2_6': 'AB3C8_1_a_ce_ghi_25',
              'AB2_2_i_i2_2': 'AB2_4_i2_i4_2',
              'AB2_1_a_i_12': 'AB2_2_e_e2_11',
              'ABC_2_a_a_a_7': 'ABC_2_a_a_a_31',
              'A_2_d_164': 'A_2_c_191',
              'AB2_4_gi_i2j_12': 'Undefined'}


def get_prototype_information(db,
                              symprec: float = 0.5):

    """
    Performs symmetry analysis of the rows in an ase database and gives each
    symmetry combination a unique name.

    Params:
        db: An ase db to be analysed for symmetry
        symprec: The precision to which the symmetry is determined in spglib

    Returns:
        prototype_labels: A str <-> str dict mapping symmetry information to
                          prototype names
        tree: A dictionary with the symmetry information as keys and with the
              corresponding rows in the database as values
    """
    rows = [row for row in db.select()]

    # We start by splitting every material in the database according to
    # stoichiometry and symmetry:
    tree = defaultdict(list)
    count = 0
    for row in rows:
        atoms = row.toatoms()

        symmetry_id = get_symmetry_id(atoms, symprec=symprec)
        if len(atoms) >= 50:
            symmetry_id = 'Perovskite'
        tree[symmetry_id].append(row)
        count += 1
        if count % 50 == 0:
            print(count)

    # And then we assign a name to each stoichiometry/symmetry combination,
    # unless another name has already been defined.
    to_delete = []
    undefined = []
    for symmetry_id in tree:
        # If we have a name, skip this symmetry
        if symmetry_id in prototype_labels or symmetry_id in exceptions:
            continue

        # Otherwise, select the most stable compound as a representative
        rows = tree[symmetry_id]
        stable_rows = [x for x in rows
                       if x.get('dynamic_stability_level', None) == "3. HIGH"]
        if not stable_rows:
            undefined += rows
            to_delete.append(symmetry_id)
            continue
        ehulls = np.array([x.get('ehull', np.inf) for x in stable_rows])
        order = ehulls.argsort()
        for i in order:
            name = get_reduced_formula(stable_rows[i].formula)
            if name not in prototype_labels.values():
                break

        # If none of the high-stability names work, try the medium stability
        # ones
        else:  # No break
            stable_rows = [x for x in rows
                           if x.get('dynamic_stability_level', None) ==
                           '2. MEDIUM']
            ehulls = np.array([x.get('ehull', np.inf) for x in stable_rows])
            order = ehulls.argsort()
            for i in order:
                name = get_reduced_formula(stable_rows[i].formula)
                if name not in prototype_labels.values():
                    break

            # Otherwise, give up.
            else:  # No break
                undefined += rows
                to_delete.append(symmetry_id)
                continue
        prototype_labels[symmetry_id] = name

    for symmetry_id in to_delete:
        del tree[symmetry_id]

    if undefined:
        tree['Undefined'] += undefined
    return tree, prototype_labels


def update_prototypes(db):
    uids = defaultdict(int)
    tree, prototype_labels = get_prototype_information(db)
    for symmetry_id in tree:
        if symmetry_id in exceptions:
            name = prototype_labels[exceptions[symmetry_id]]
        else:
            name = prototype_labels[symmetry_id]

        for row in tree[symmetry_id]:
            uid = '-'.join([row.formula, name, row.magstate])
            uids[uid] += 1
            if uids[uid] > 1:
                uid += "-" + str(uids[uid])
            if uid != getattr(row, 'uid', None):
                db.update(row.id, prototype=name, uid=uid)


if __name__ == "__main__":
    import sys
    db_file = sys.argv[1]
    db = connect(db_file)
    update_prototypes(db)
