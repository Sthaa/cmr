# Creates: bs.pdf

import numpy as np
import matplotlib.pyplot as plt
from sys import argv

tag = argv[1]

d = np.load('Thomas/data/' + tag + '_bands.npz')
x, X, e_skn, e_mk, s_kvn = d['x'], d['X'], d['e_skn'], d['e_mk'], d['s_kvn']
Ns = e_skn.shape[0]
delta = -5.0
e_mk += delta
e_skn += delta
s_kvn += -0.5
s_kvn *= 2.0

if Ns == 2:
    for e_k in e_skn[0].T:
        plt.plot(x, e_k, '--', c='r')
    for e_k in e_skn[1].T:
        plt.plot(x, e_k, '--', c='b')
if Ns == 1:
    for i, e_k in enumerate(e_skn[0].T):
        if i == 0:
            label = 'PBE'
        else:
            label = None
        plt.plot(x, e_k, '--', c='0.1', lw=2, label=label)

path = 'GKMG'
plt.xticks(X, [r'$\Gamma$', r'$\mathrm{K}$', r'$\mathrm{M}$', r'$\Gamma$'],
           size=20)
plt.yticks(size=20)
for i in range(len(X))[1:-1]:
    plt.plot(2 * [X[i]], [1.1 * np.min(e_mk), 1.1 * np.max(e_mk)],
             c='0.1', linewidth=0.75)

plt.scatter(np.tile(x, len(e_mk)), e_mk.reshape(-1),
            c=s_kvn[:, 2].T.reshape(-1),
            s=4,
            marker='+')

plt.colorbar()
plt.ylabel(r'$\varepsilon-\varepsilon_{\mathrm{vac}}$ (eV)', size=24)
plt.axis([0, x[-1], -2.5 + delta, 2.5 + delta])
plt.legend(fontsize=12)
plt.tight_layout()
plt.savefig('bs.pdf')
plt.show()
