from __future__ import print_function

import os
from pathlib import Path
import numpy as np
from ase.parallel import parprint, rank, broadcast
from ase.io import read, Trajectory
from ase.io.formats import UnknownFileTypeError
from gpaw import GPAW, restart
from c2db.utils import gpw2eigs
from c2db.em import _readmass


def strained_gpw_name(gpw, strain, direction):
    """
    Canonically transform a gpw name into a filename taking into account strain
    - both the amount and the direction

    """
    if strain == 0:
        return gpw
    if direction == 'x':
        dir_name = 'xx'
    elif direction == 'y':
        dir_name = 'yy'
    elif direction == 'xy':
        dir_name = 'xxyy'
    gpw_basename, gpw = os.path.splitext(gpw)
    new_gpw_name = ("{name}_strained_{strain}_"
                    "{dir_name}.gpw".format(name=gpw_basename,
                                            strain=strain,
                                            dir_name=dir_name))
    return new_gpw_name


def restart_strained_system(atoms, traj_name):
    if not Path(traj_name).is_file():
        return atoms
    structure = None
    error = False
    if rank == 0:
        try:
            structure = read(traj_name, parallel=False)
        except (IOError, UnknownFileTypeError):
            error = True
    error = broadcast(error)
    if error:
        return atoms

    return broadcast(structure)


def calculate_strained_energies(gpw,
                                strain_percent=1.0,
                                direction='x',
                                lattice_relax=False,
                                ionic_relax=False,
                                symmetrize_density=False):
    """
    Calculate the energies and wavefunctions of a structure after
    applying strain in a given direction. Save this information to a gpw
    file for post-processing.

    params:
        gpw_name (str): The filename of a completed GPAW calculation on a
            densely sampled k-point grid.
        strain_percent (float): How much to strain the structure by, in percent
        direction (str): Which direction to strain the structure in.
            Valid arguments are 'x', 'y' and 'xy'
        lattice_relax (bool): Whether to allow in-plane relaxations of the
            lattice constant in the direction not being strained, due to
            Poisson's ratio. Only relevant for uniaxial strains.
        ionic_relax (bool): Whether to allow out-of-plane relaxations.
    """
    atoms, calc = restart(gpw, txt=None)
    gpw_basename, gpw = os.path.splitext(gpw)
    new_gpw_name = strained_gpw_name(gpw_basename, strain_percent, direction)
    cell = atoms.get_cell()
    # z = cell[2, 2]
    strain = np.eye(3)
    if direction == 'x':
        strain[0, 0] += 0.01 * strain_percent
        mask = np.array([0, 1, 0, 0, 0, 0])
    elif direction == 'y':
        strain[1, 1] += 0.01 * strain_percent
        mask = np.array([1, 0, 0, 0, 0, 0])
    elif direction == 'xy':
        strain[0, 0] += 0.01 * strain_percent
        strain[1, 1] += 0.01 * strain_percent
        mask = np.array([0, 0, 0, 0, 0, 0])
    if not lattice_relax:
        mask = np.array([0, 0, 0, 0, 0, 0])
    new_cell = np.dot(cell, strain)
    atoms.set_cell(new_cell, scale_atoms=True)
    traj_name = new_gpw_name.replace('.gpw', '.traj')
    atoms = restart_strained_system(atoms,
                                    traj_name)

    parameters = calc.todict()
    parameters['fixdensity'] = False
    parameters['txt'] = new_gpw_name.replace(".gpw", ".txt")
    if not symmetrize_density:
        if 'symmetry' not in parameters:
            parameters['symmetry'] = {'do_not_symmetrize_the_density': True}
        elif isinstance(parameters['symmetry'], dict):
            parameters['symmetry']['do_not_symmetrize_the_density'] = True
    new_calc = GPAW(**parameters)
    atoms.set_calculator(new_calc)
    atoms.get_potential_energy()
    
    from ase.optimize import BFGS
    if ionic_relax or mask.any():  # We need to do some relaxations
        if ionic_relax:
            if np.array(mask).any():
                from ase.constraints import UnitCellFilter
                constrained = UnitCellFilter(atoms, mask)
            else:
                constrained = atoms
        else:
            from ase.constraints import StrainFilter
            constrained = StrainFilter(atoms, mask)

        relax = BFGS(constrained,
                     trajectory=Trajectory(traj_name, 'a', atoms))
        relax.run(fmax=0.008)
        
    atoms.get_forces()
    try:
        atoms.get_stress()
        new_calc.write(new_gpw_name)
    except TypeError:
        new_calc.write(new_gpw_name)
        calc = GPAW(new_calc, txt=None)
        calc.get_stress()
        calc.write(new_gpw_name)


def get_matching_energies(gpw, skn1, skn2, soc=False):
    """
    Find the energy of the band corresponding to a given
    k-point, band index and sz expectation value

    params:
        calc: A GPAW calculator object with the desired wavefunctions.
        skn1 (float, int, int): the expectation value of <sz>, the band
            index, n, and the kpt index of the vbm.
        skn2 (float, int, int): the expectation value of <sz>, the band
            index, n, and the kpt index of the cbm.
        soc: Whether or not to use spin orbit coupling. If not False, it should
            contain an array of spin orbit eigenvalues and <sz> expectation
            values.
    returns:
        energies (float, float): The energies of the new calculator at the
            spin, band and kpt indices corresponding to skn1 and skn2
            respectively.
    """

    energies = []
    calc = GPAW(gpw, txt=None)
    e_vac = calc.get_electrostatic_potential().mean(0).mean(0)[0]
    if soc:
        e_km, _, s_kvm = gpw2eigs(gpw, soc=True, return_spin=True,
                                  optimal_spin_direction=True)
        for idx, skn in enumerate([skn1, skn2]):
            sz_value, kpt, band = skn
            band, kpt = int(band), int(kpt)
            kpt = calc.wfs.kd.bz2ibz_k[kpt]
            # check the target band, as well as one higher (for cbm), or one
            # lower (for vbm).
            target_bands = (band, band - 1 + 2 * idx)
            new_energies = e_km[kpt, target_bands]
            new_spins = s_kvm[kpt, 2, target_bands]
            spin_index = np.abs(new_spins - sz_value).argmin()
            energy = new_energies[spin_index]
            energies.append(energy)
    else:
        for idx, skn in enumerate([skn1, skn2]):
            s, k, n = skn
            k = calc.wfs.kd.bz2ibz_k[k]
            e_n = calc.get_eigenvalues(kpt=k, spin=s)
            energy = e_n[n]
            energies.append(energy)
    return np.array(energies) - e_vac


def calculate_strained_edges(gpw='densk.gpw',
                             strain_percent=1.0,
                             gap_filename=None,
                             lattice_relax=False,
                             ionic_relax=True,
                             soc=True):
    """
    Calculate the band edge energies of a system after straining in the xx, the
    yy and the xxyy directions.

    params:
        gpw (str): The filename of the gpw restart file of a ground state
            calculation of the unstrained system.
        strain_percent (float): How much to strain the system by
        gap_filename (str): The filename of band edge information for the
            unstrained system. If this is None, the function looks in a
            standard location governed by ``soc''.
        soc (bool): Whether to use spin orbit coupling or not.
    returns:
        edges: a 3x2 numpy array containing the energy of the (VBM, CBM) with
            respect to vacuum for strain in the x direction, strain in y and
            biaxial strain.
    """
    if gap_filename is not None:
        data = np.load(gap_filename)
    elif soc:
        data = np.load('gap_soc.npz')
    else:
        data = np.load('gap.npz')
    if data['gap'] == 0.0:
        return None
    vbm = data['vbm']
    cbm = data['cbm']
    edges = np.zeros((3, 2))
    skn1 = np.array(data['skn1'])
    skn2 = np.array(data['skn2'])
    calc = GPAW(gpw, txt=None)
    if strain_percent == 0.0:
        # The pristine system and the different strained systems have slightly
        # different vacuum levels.
        old_vacuum = calc.get_electrostatic_potential().mean(0).mean(0)[0]
        edges[:, 0] = vbm - old_vacuum
        edges[:, 1] = cbm - old_vacuum
        return edges
    if soc:
        _, _, s_kvm = gpw2eigs(gpw, soc=True, return_spin=True,
                               optimal_spin_direction=True)
        s1, k1, n1 = skn1
        s2, k2, n2 = skn2
        sz1 = s_kvm[k1, 2, n1]
        sz2 = s_kvm[k2, 2, n2]
        skn1, skn2 = skn1.astype(float), skn2.astype(float)
        skn1[0], skn2[0] = sz1, sz2

    # The strained calculation can have a lower symmetry than the pristine one.
    # Therefore, the mappings BZ<->IBZ can be different, even with the same k
    # point sampling. We therefore get the BZ_k index, and use that to identify
    # the band edges.
    skn1[1] = calc.wfs.kd.ibz2bz_k[int(skn1[1])]
    skn2[1] = calc.wfs.kd.ibz2bz_k[int(skn2[1])]

    for idx, direction in enumerate(['x', 'y', 'xy']):
        new_gpw = strained_gpw_name(gpw, strain_percent, direction)
        if not os.path.isfile(new_gpw):
            calculate_strained_energies(gpw,
                                        strain_percent,
                                        direction,
                                        lattice_relax=lattice_relax,
                                        ionic_relax=ionic_relax)
        strained_energies = get_matching_energies(new_gpw, skn1, skn2, soc=soc)
        edges[idx] = np.array(strained_energies)
    return edges


def mobility(c_11, m_x, m_y, E_elph, T=300):
    from ase.units import kB, J, _hbar, _me
    '''
    Calculate the electron mobility based on the Takagi formalism.

    Params:
        c_11: The bulk modulus of the material, in N/m
        m_x: The electron mass in the x direction, in a.u.
        m_y: The electron mass in the y direction, in a.u.
        E_elph: The electron-phonon coupling, in eV
        T: The temperature in K

    Returns:
        mu, the mobility of the material in cm^2/(Vs)
    '''

    if m_x * m_y < 0:
        parprint("Warning! The effective masses in the x and y directions "
                 "for have different signs. The corresponding mobilities "
                 "are therefore meaningless, as the DOS mass is "
                 "imaginary.")
        return np.nan
    m_d = np.sqrt(m_x * m_y)
    prefactor = _hbar**3 / (kB * _me * _me / J**2)
    mu = prefactor * c_11 / (T * m_x * m_d * E_elph**2)
    return mu * 1e4


def collect_deformation_potentials(gpw='densk.gpw',
                                   strains=[-1.0, 1.0],
                                   lattice_relax=False,
                                   ionic_relax=True,
                                   data={}):
    """
    Calculate the deformation potential both with and without spin orbit
    coupling, for both the conduction band and the valence band, and return as
    a dictionary.
    """
    parprint('Calculating deformation potentials')
    if 0.0 not in strains:
        strains.append(0.0)
    strains = sorted(strains)
    new_data = {'strains': np.array(strains)}
    for soc in (True, False):
        bg = True
        edges = list(range(len(strains)))
        for idx, strain in enumerate(strains):
            x = calculate_strained_edges(soc=soc,
                                         gpw=gpw,
                                         lattice_relax=lattice_relax,
                                         ionic_relax=ionic_relax,
                                         strain_percent=strain)
            if x is None:
                bg = False
                break
            edges[idx] = x
        if not bg:
            continue

        edges = np.array(edges)
        deformation_potentials = np.zeros(np.shape(edges)[1:])
        for idx, band_edge in enumerate(['vbm', 'cbm']):
            D = np.polyfit(strains, edges[:, :, idx], 1)[0] * 100
            D[2] /= 2
            deformation_potentials[:, idx] = D
        new_data[['edges_nosoc', 'edges'][soc]] = edges
        new_data[['deformation_potentials_nosoc',
                  'deformation_potentials'][soc]] = deformation_potentials
    data.update(**new_data)
    return data


def calculate_2D_stiffness_tensor(gpw='densk.gpw',
                                  strain_percent=1.0,
                                  ionic_relax=True,
                                  lattice_relax=False,
                                  data={}):
    parprint('Calculating stiffness tensor')
    from ase.units import J
    atoms, calc = restart(gpw, txt=None)
    z = atoms.get_cell()[2, 2]
    atoms = calc.atoms
    cell = atoms.get_cell()
    z = cell[2, 2]
    stiffness = np.zeros((2, 2))
    for idx, direction in enumerate(['x', 'y']):
        stress_2D = np.zeros(2)
        for sign in [-1, 1]:
            strain = strain_percent * sign
            new_gpw = strained_gpw_name(gpw,
                                        strain,
                                        direction)
            if not os.path.isfile(new_gpw):
                calculate_strained_energies(gpw,
                                            strain_percent=strain,
                                            direction=direction,
                                            ionic_relax=ionic_relax,
                                            lattice_relax=lattice_relax)
            atoms, calc = restart(new_gpw, txt=None)
            stress = atoms.get_stress()
            stress_2D += stress[[0, 1]] * z * sign
        stiffness[idx] = stress_2D / (strain_percent * 0.02)
    stiffness *= 10**20 / J
    data['stiffness_tensor'] = stiffness

    from ase.units import kg
    from ase.units import m as meter
    cell = atoms.get_cell()
    area = atoms.get_volume() / cell[2, 2]
    mass = sum(atoms.get_masses())
    area_density = (mass / kg) / (area / meter**2)
    # speed of sound in m/s
    speed_x = np.sqrt(stiffness[0, 0] / area_density)
    speed_y = np.sqrt(stiffness[1, 1] / area_density)
    speed_of_sound = np.array([speed_x, speed_y])
    data['speed_of_sound'] = speed_of_sound
    return data


def collect_mobilities(data={}):
    parprint('Calculating mobilities')

    def align_masses(masses, eigenvectors):
        theta = np.arccos(eigenvectors[:, 0])
        m_x = (masses[0] * masses[1] / (np.cos(theta)**2 * masses[1]
                                        + np.cos(theta)**2 * masses[0]))
        m_y = (masses[0] * masses[1] / (np.cos(theta)**2 * masses[0]
                                        + np.cos(theta)**2 * masses[1]))

        return m_x, m_y

    for soc in [True, False]:
        name = '' if soc else '_nosoc'
        if 'deformation_potentials' + name not in data:
            continue
        E = data['deformation_potentials' + name]
        stiffness_tensor = data['stiffness_tensor']
        c_11 = stiffness_tensor[0, 0]
        c_22 = stiffness_tensor[1, 1]

        d_e = _readmass(soc, 'cb')
        d_h = _readmass(soc, 'vb')
        m_ex, m_ey = align_masses(d_e[d_e['indices'][0]]['m_u'],
                                  d_e[d_e['indices'][0]]['eigenvectors_vu'])
        m_hx, m_hy = align_masses(d_h[d_h['indices'][0]]['m_u'],
                                  d_h[d_h['indices'][0]]['eigenvectors_vu'])
        E_elph = E[:2, 1]
        mu_ex = mobility(c_11, m_ex, m_ey, E_elph[0])
        mu_ey = mobility(c_22, m_ex, m_ey, E_elph[1])
        E_hoph = E[:2, 0]
        mu_hx = mobility(c_11, m_hx, m_hy, E_hoph[0])
        mu_hy = mobility(c_22, m_hx, m_hy, E_hoph[1])
        data['electron_mobilities' + name] = mu_ex, mu_ey
        data['hole_mobilities' + name] = mu_hx, mu_hy
    return data
