# creates: dependencies.csv
from pathlib import Path

fd = open('dependencies.csv', 'w')
for line in Path('submit.py').read_text().splitlines():
    if 'rtask(' in line:
        name = line.split('@')[0].split('c2db.')[1]
        if 'deps=' in line:
            dep = line.split('c2db.')[-1].split("'")[0]
        else:
            dep = ''
        files = []
        for line in Path(name + '.py').read_text().splitlines():
            if line.startswith('# Creates:'):
                files += line.split(': ')[1].split(', ')
            if line.startswith('# temporary:'):
                files += [f'({f})' for f in line.split(': ')[1].split(', ')]
        files = ', '.join(files)
        print(f'{name}, {dep}, "{files}"', file=fd)
