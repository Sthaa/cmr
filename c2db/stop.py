# Creates: stop

import os
from pathlib import Path
import numpy as np
from ase.io import read
from c2db.references import formation_energy


def stop_workflow(hform_criterion=0.2, hessian_criterion=-3):
    state = Path().cwd().parts[-1]
    assert state in ['nm', 'fm', 'afm']
    name = '../relax-{}.traj'.format(state)
    atoms = read(name)
    hform = formation_energy(atoms) / len(atoms)
    result = []
    if hform > hform_criterion:
        result.append("Heat of formation is {:.2f}!\n".format(hform))
    if os.path.isfile('c2db.phonons.done'):
        from c2db.phonons import analyse
        eigs, freqs = analyse(atoms)
        mineig = eigs.min()
        if mineig < hessian_criterion:
            result.append("Min hessian eigenvalue is {:.2f}!\n".format(mineig))
    if os.path.isfile('strain_quantities.npz'):
        d = np.load('strain_quantities.npz')
        stiffness = d['stiffness_tensor']
        c_11, c_22 = stiffness[0, 0], stiffness[1, 1]
        if c_11 < 0:
            result.append("C11 is {:.2f}!\n".format(c_11))
        if c_22 < 0:
            result.append("C22 is {:.2f}!\n".format(c_22))
    return result


if __name__ == "__main__":
    import sys
    from c2db import chdir
    if len(sys.argv) == 1:
        folders = ['.']
    else:
        folders = sys.argv[1:]
    for folder in folders:
        with chdir(folder):
            result = stop_workflow()
            if result:
                with open('stop', 'w') as f:
                    for line in result:
                        f.write(line)
                raise ValueError("Unstable material found. "
                                 "Stopping workflow")
