import os
import numpy as np
from ase.utils import seterr

import c2db


def get_p_ab(s_ab):
    """
    The data is saved as a matrix of counts, so that s_ab gives the number of
    times that a can substitute for b in the icsd. This is be normalized,
    to give a probability of succesful substitution. See

    """
    with seterr(divide='ignore', invalid='ignore'):
        tmp = s_ab ** 2 / s_ab.sum(axis=0)[None, :] / s_ab.sum(axis=1)[:, None]
    tmp[np.isnan(tmp)] = 0
    np.fill_diagonal(tmp, 1)
    return np.sqrt(tmp)


folder = os.path.dirname(c2db.__file__)
s_ab = np.loadtxt(os.path.join(folder, 'substitution.dat'))
p_ab = get_p_ab(s_ab)
