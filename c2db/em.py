# Creates: em_circle_cb_nosoc.gpw, em_circle_cb_soc.gpw
# Creates: em_circle_vb_nosoc.gpw, em_circle_vb_soc.gpw
# Creates: em_circle_cb_nosoc.npz, em_circle_cb_soc.npz
# Creates: em_circle_vb_nosoc.npz, em_circle_vb_soc.npz
import traceback
from collections import namedtuple
from typing import List, Dict, Tuple, Optional, Union


from scipy import optimize
import numpy as np

from ase.dft.kpoints import kpoint_convert
from ase.dft.bandgap import bandgap
from ase.parallel import parprint as print
from ase.units import Bohr, Hartree
from gpaw import GPAW

import os.path as op
from c2db.utils import gpw2eigs
la = np.linalg


SB = namedtuple('SB', ('spin', 'band'))

MassDict = Dict[str, Union[np.ndarray, int]]
MassesDict = Dict[Union[SB, str], Union[MassDict, List[SB]]]


def print_coeff(coeff) -> None:
    names = ['kx^2', 'ky^2', 'kx ky', 'kx', 'ky', '1',  # second order
             'kx^3', 'ky^3', 'kx^2 ky', 'ky^2 kx']  # third order
    for cstr, c in zip(names, coeff):
        print('{} {:.2e}'.format(cstr, c))


def model(kpts_kv):
    """ simple third order model
        Parameters:
            kpts_kv: (nk, 3)-shape ndarray
                units of (1 / Bohr)
    """
    k_kx, k_ky = kpts_kv[:, 0], kpts_kv[:, 1]
    ones = np.ones(len(k_kx))
    # A_dp (# of data points, # of model parameters)-shape array
    A_dp = np.array([k_kx**2,
                     k_ky**2,
                     k_kx * k_ky,
                     k_kx,
                     k_ky,
                     ones,
                     k_kx**3,
                     k_ky**3,
                     k_kx**2 * k_ky,
                     k_ky**2 * k_kx]).T
    return A_dp


def evalmodel(kpts_kv, c_p):
    """energy eigenvalue for a model for some parameters c_p
    Parameters:
        kpts_kv: (3,)-shape or (nk, 3)-shape ndarray
            kpts in cartesian coordinates (1 / Bohr)
        c_p: (np,)-shape ndarray
            coefficients for the np paramaters in the model
    """
    kpts_kv = np.asarray(kpts_kv)
    if kpts_kv.ndim == 1:
        kpts_kv = kpts_kv[np.newaxis]
    A_kp = model(kpts_kv)
    return np.dot(A_kp, c_p)


def fit(kpts_kv, eps_k, thirdorder: bool = False):
    A_kp = model(kpts_kv)
    if not thirdorder:
        A_kp = A_kp[:, :6]
    c, r, rank, s = la.lstsq(A_kp, eps_k, rcond=-1)
    return c, r, rank, s


def em(kpts_kv, eps_k,
       bandtype: Optional[str] = None) -> MassDict:
    """
    Parameters:
        kpts_kv: (nk, 3)-shape ndarray
            k-points in cartesian coordinates (in units of 1 / Bohr)
        eps_k: (nk,)-shape ndarray
            eigenvalues (in units of Hartree)
    Returns:
        out: dct
            - effective masses in units of m_e
            - eigenvectors in cartesian coordinates
            - k-pot extremum in cartesian coordinates (units of 1 / Bohr)

    """
    # first a second order fit to get started
    c, r, rank, s = fit(kpts_kv, eps_k, thirdorder=False)
    fxx = 2 * c[0]
    fyy = 2 * c[1]
    fxy = c[2]
    # assert fxx * fyy - fxy**2 > 0, 'probably found a saddle saddle-point'

    def get_bt(fxx, fyy, fxy):
        if fxx * fyy - fxy**2 > 0:
            bandtype = 'sadlepoint'
        elif fxx < 0 and fyy < 0:  # a max
            bandtype = 'vb'
        elif fxx > 0 and fyy > 0:  # a min
            bandtype = 'cb'
        return bandtype

    if bandtype is None:
        bandtype = get_bt(fxx, fyy, fxy)
        print('2nd order: it is a {}m we are after'.format(bandtype))
    # (kx, ky) extremum from from second order fit
    d = 4 * c[0] * c[1] - c[2]**2
    x = (-2 * c[1] * c[3] + c[2] * c[4]) / d
    y = (-2 * c[0] * c[4] + c[2] * c[3]) / d
    x20 = np.array((x, y, 0.0))  # extremum from second order fit
    # print('em.em using the extremum from 2nd order (kx, ky): ', x20[:2])
    # third order
    c3, r3, rank3, s3 = fit(kpts_kv, eps_k, thirdorder=True)
    f3xx = 2 * c3[0] + 6 * c3[6] * x + 2 * c3[8] * y
    f3yy = 2 * c3[1] + 6 * c3[7] * y + 2 * c3[9] * x
    f3xy = c3[2] + 2 * c3[8] * x + 2 * c3[9] * y
    if bandtype is None:
        bandtype = get_bt(f3xx, f3yy, f3xy)
    # try to find the (kx, ky) extremum using the third order fit
    sign = -1 if bandtype == 'vb' else 1
    x, y, _ = optimize.fmin(evalmodel, x0=x20, args=(sign * c3,),
                            xtol=1.0e-15, ftol=1.0e-15, disp=False)
    x30 = np.array((x, y, 0.0))  # extremum from thirdorder fit
    print('em.em found the extremum from third order fit (kx, ky): ', x30[:2])
    f3xx = 2 * c3[0] + 6 * c3[6] * x + 2 * c3[8] * y
    f3yy = 2 * c3[1] + 6 * c3[7] * y + 2 * c3[9] * x
    f3xy = c3[2] + 2 * c3[8] * x + 2 * c3[9] * y
    # from second order fit
    M2_vv = [[fxx, fxy],
             [fxy, fyy]]
    # from third order fit
    M3_vv = [[f3xx, f3xy],
             [f3xy, f3yy]]
    v2_n, w2_vn = la.eigh(M2_vv)
    v3_n, w3_vn = la.eigh(M3_vv)
    print('em.em 1 / v2_n', 1 / v2_n)
    print('em.em 1 / v3_n', 1 / v3_n)
    out = dict(mass_u=1 / v3_n, eigenvectors_vu=w3_vn,
               ke_v=x30,
               ke2_v=x20,
               mass2_u=1 / v2_n, eigenvectors2_vu=w2_vn,
               c=c3,
               c2=c,
               r=r3,
               r2=r)
    return out


def get_vb_cb_indices(e_skn,
                      efermi: float,
                      delta: float) -> Tuple[List[SB], List[SB]]:
    """
    find CB and VB within a distance of delta of the CB and VB extrema
    Parameters:
        e_skn: (ns, nk, nb)-shape ndarray
            eigenvalues
        efermi: float
            fermi level
        delta: float
            bands within delta of the extrema are included
    Returns:
        vb_indices, cb_indices: [(spin, band), ..], [(spin, band), ...]
            spin and band indices (aka as SBandex) for VB and CB, respectively
    """
    if e_skn.ndim == 2:
        e_skn = e_skn[np.newaxis]

    gap, (s1, k1, n1), (s2, k2, n2) = bandgap(eigenvalues=e_skn, efermi=efermi,
                                              output=None)
    if not gap > 0:
        raise ValueError('Band gap is zero!!!')
    cbm = e_skn[s2, k2, n2]
    vbm = e_skn[s1, k1, n1]
    cb_sn = e_skn[:, k2, n2:]      # cb's at k_cbm
    vb_sn = e_skn[:, k1, :n1 + 1]  # cb's at k_cbm
    cbs, cbn = np.where(cb_sn <= cbm + delta)
    cbn += n2
    cb_indices = [SB(s, b) for s, b in (zip(cbs, cbn))]
    vbs, vbn = np.where(vb_sn >= vbm - delta)
    vb_indices = list(reversed([SB(s, n) for s, n in (zip(vbs, vbn))]))
    return vb_indices, cb_indices


def embands(gpw: str,
            soc: bool,
            bandtype: str,
            efermi: Optional[float] = None,
            delta: float = 0.1) -> MassesDict:
    """effective masses for bands within delta of extrema
    Parameters:
        gpw: str
            name of gpw filename
        soc: bool
            include spin-orbit coupling
        bandtype: 'vb' or 'cb'
            type of band
        efermi: float, optional
            fermi level (takes it from gpw if None)
        delta: float, optional
            bands within this value (in eV) is included in the em fit
            default is 0.1 eV
    """
    calc = GPAW(gpw, txt=None)
    e_skn, efermi2 = gpw2eigs(gpw, soc=soc, optimal_spin_direction=True)
    if efermi is None:
        efermi = efermi2
    if e_skn.ndim == 2:
        e_skn = e_skn[np.newaxis]  # dummy spin for spinorbit
    vb_ind, cb_ind = get_vb_cb_indices(e_skn=e_skn, efermi=efermi, delta=delta)
    indices = vb_ind if bandtype == 'vb' else cb_ind
    atoms = calc.get_atoms()
    cell_cv = atoms.get_cell()
    ibz_kc = calc.get_ibz_k_points()
    ibz_kv = kpoint_convert(cell_cv=cell_cv, skpts_kc=ibz_kc)
    masses = {'indices': indices}  # type MassesDict
    for b in indices:
        e_k = e_skn[b.spin, :, b.band]
        print('-' * 79 + '\nembands: b={}, soc={}'.format(b, soc) +
              '\n' + '-' * 79)
        masses[b] = em(kpts_kv=ibz_kv * Bohr,
                       eps_k=e_k / Hartree, bandtype=bandtype)
    return masses


def kptsincircle(cell_cv, npoints=9, erange=1e-3, m=1.0):
    a = np.linspace(-1, 1, npoints)
    X, Y = np.meshgrid(a, a)
    indices = X**2 + Y**2 <= 1
    x, y = X[indices], Y[indices]
    kpts_kv = np.vstack([x, y, [0.] * len(x)]).T
    kr = np.sqrt(2 * m * erange / Hartree)  # in 1 / Bohr
    kpts_kv *= kr
    kpts_kv /= Bohr  # now in 1 / Angstrom
    kpts_kc = kpoint_convert(cell_cv=cell_cv, ckpts_kv=kpts_kv)
    return kpts_kc


def nonsc_circle(gpw='densk.gpw', soc=False, bandtype=None):
    """non sc calculation based for kpts in a circle around the
        valence band maximum and conduction band minimum.
        writes the files:
            em_circle_vb_soc.gpw
            em_circle_cb_soc.gpw
            em_circle_vb_nosoc.gpw
            em_circle_cb_nosoc.gpw
        Parameters:
            gpw: str
                gpw filename
            soc: bool
                spinorbit coupling
            bandtype: None or 'cb' or 'vb'
                which bandtype do we do calculations for, if None is done for
                for both cb and vb

    """
    calc = GPAW(gpw, txt=None)
    k_kc = calc.get_ibz_k_points()
    cell_cv = calc.atoms.get_cell()
    kcirc_kc = kptsincircle(cell_cv)
    e_skn, efermi = gpw2eigs(gpw, soc=soc, optimal_spin_direction=True)
    if e_skn.ndim == 2:
        e_skn = e_skn[np.newaxis]
    _, (s1, k1, n1), (s2, k2, n2) = bandgap(eigenvalues=e_skn, efermi=efermi,
                                            output=None)
    k1_c = k_kc[k1]
    k2_c = k_kc[k2]
    if bandtype is None:
        bandtypes = ('vb', 'cb')
        ks = (k1_c, k2_c)
    elif bandtype == 'vb':
        bandtypes = ('vb', )
        ks = (k1_c, )
    elif bandtype == 'cb':
        bandtypes = ('cb', )
        ks = (k2_c, )

    for bt, k_c in zip(bandtypes, ks):
        name = get_name(soc=soc, bt=bt)
        print('em.nonsc_circle: k_c=', k_c)
        calc.set(kpts=kcirc_kc + k_c,
                 symmetry='off',
                 txt=name + '.txt')
        atoms = calc.get_atoms()
        atoms.get_potential_energy()
        calc.write(name + '.gpw')


def get_name(soc, bt):
    return 'em_circle_{}_{}'.format(bt, ['nosoc', 'soc'][soc])


def _savemass(soc: bool, bt: str, mass: MassesDict):
    from ase.parallel import world
    fname = get_name(soc, bt) + '.npz'
    if world.rank == 0:
        mass2 = {}  # type Dict[Union[Tuple, str], Any]
        for k, v in mass.items():
            if type(k) == SB:
                mass2[tuple(k)] = v
            elif k == 'indices':
                mass2[k] = [tuple(vi) for vi in v]
            else:
                mass2[k] = v
        with open(fname, 'wb') as f:
            np.savez(f, data=mass2)
    world.barrier()


def _readmass(soc, bt):
    fname = get_name(soc=soc, bt=bt) + '.npz'
    with open(fname, 'rb') as f:
        dct = dict(np.load(f))['data'].tolist()
    dct2 = {}  # type MassesDict
    for k, v in dct.items():
            if isinstance(k, tuple) and len(k) == 2:
                dct2[SB(*k)] = v
            elif k == 'indices':
                dct2[k] = [SB(*vi) for vi in v]
            else:
                dct2[k] = v
    return dct2


def runem(socs=(True, False), bandtypes=('vb', 'cb'), gpw='densk.gpw'):
    for soc in socs:
        eigenvalues, efermi = gpw2eigs(gpw=gpw, soc=soc,
                                       optimal_spin_direction=True)
        gap, _, _ = bandgap(eigenvalues=eigenvalues, efermi=efermi,
                            output=None)
        if not gap > 0:
            continue
        for bt in bandtypes:
            name = get_name(soc=soc, bt=bt)
            gpw2 = name + '.gpw'
            if not op.isfile(gpw2):
                nonsc_circle(soc=soc, bandtype=bt)
            try:
                masses = embands(gpw2,
                                 soc=soc,
                                 bandtype=bt,
                                 efermi=efermi)
            except ValueError:
                tb = traceback.format_exc()
                print(gpw2 + ':\n' + '=' * len(gpw2) + '\n', tb)
            else:
                _savemass(soc=soc, bt=bt, mass=masses)


if __name__ == '__main__':
    runem(socs=(True, False))
