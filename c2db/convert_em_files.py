"""add indices to em_circle_*.npz files
"""
import os
from c2db.em import get_vb_cb_indices, _readmass, _savemass, get_name
from c2db.utils import gpw2eigs
import traceback


def add_sb_indices() -> None:
    socs = [True, False]
    bts = ['vb', 'cb']
    gpw = 'densk.gpw'

    for soc in socs:
        e_km, efermi = gpw2eigs(gpw, soc=soc)
        vb_indices, cb_indices = get_vb_cb_indices(e_skn=e_km,
                                                   efermi=efermi,
                                                   delta=0.1)
        for bt in bts:
            indices = vb_indices if bt == 'vb' else cb_indices
            mdict = _readmass(soc=soc, bt=bt)
            if 'indices' in mdict:
                continue
            mdict2 = {'indices': indices}
            for sb in indices:
                assert sb in mdict

            mdict2.update(mdict)
            name = get_name(soc=soc, bt=bt)
            print(name)
            os.rename(name + '.npz', name + '.bak.npz')
            _savemass(soc=soc, bt=bt, mass=mdict2)


if __name__ == '__main__':
    import sys
    from c2db import chdir

    folders = sys.argv[1:]
    errors = []
    for folder in folders:
        with chdir(folder):
            try:
                add_sb_indices()
            except Exception as x:
                error = '{}: {}'.format(x.__class__.__name__, x)
                tb = traceback.format_exc()
                error = (folder, error, tb)
                errors.append(error)
                print('E', end='')
            else:
                print('.', end='')
    if len(errors):
        print('\nERRORS:')
        for error in errors:
            print(*error)
