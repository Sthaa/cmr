# Creates: vacuumlevels.gpw, vacuumlevels.npz
from gpaw import GPAW
from ase.parallel import world
import numpy as np
from ase.units import Bohr, Hartree
from ase.utils import basestring
from ase.io import read


def evacdiff(atoms):
    """Calculate vacuum energy level difference from the dipole moment of
    a slab assumed to be in the xy plane
    Parameters:
            atoms: Atoms object or str
                str name of file that can be read by ase.io.read
    Returns:
        out: float
            vacuum level difference in eV
    """
    if isinstance(atoms, basestring):
        atoms = read(atoms)
    A = np.linalg.det(atoms.cell[:2, :2] / Bohr)
    dipz = atoms.get_dipole_moment()[2] / Bohr
    evacsplit = 4 * np.pi * dipz / A
    return evacsplit * Hartree


def dftdipcorrected(gpw='gs.gpw', evacdiffmin=10e-3):
    """ dipole corrected dft calculation -> vacuumlevels.npz
    Parameters:
        gpw: str
            name of gpw file to base the dipole corrected calc on
        evacdiffmin: float
            thresshold in eV for doing a dipole moment corrected
            dft calculations if the predicted evac difference is less
            than this value don't do it
    """
    calc = GPAW(gpw, txt='vacuumlevels.txt')
    atoms = calc.get_atoms()
    p = calc.todict()
    if p.get('poissonsolver', {}) == {'dipolelayer': 'xy'}:
        vacuumlevels(gpw=gpw)
    elif abs(evacdiff(atoms)) < evacdiffmin:
        vacuumlevels(gpw=gpw)
    else:
        calc.set(poissonsolver={'dipolelayer': 'xy'})
        atoms = calc.get_atoms()
        atoms.get_potential_energy()
        atoms.get_forces()
        atoms.get_stress()
        calc.write('vacuumlevels.gpw')
        vacuumlevels(gpw='vacuumlevels.gpw')


def vacuumlevels(gpw='vacuumlevels.gpw', n=8):
    """Calculate the vacuumlevels
    Parameters:
        gpw: str
            name of gpw file
        n: int
            number of gridpoints away from the edge to evaluate the vac levels
    """
    calc = GPAW(gpw, txt=None)
    atoms = calc.get_atoms()
    efermi_nosoc = calc.get_fermi_level()
    v = calc.get_electrostatic_potential().mean(0).mean(0)
    z = np.linspace(0, atoms.cell[2, 2], len(v), endpoint=False)
    evac1 = v[n]
    evac2 = v[-n]
    ediff = evacdiff(atoms)
    dipz = atoms.get_dipole_moment()[2]
    if world.rank == 0:
        with open('vacuumlevels.npz', 'wb') as f:
            np.savez(f, vz=v, evac1=evac1, evac2=evac2, z=z,
                     efermi_nosoc=efermi_nosoc, evacdiff=ediff, dipz=dipz)


if __name__ == '__main__':
    dftdipcorrected()
