"""Workflow for C2DB project."""
import functools
from pathlib import Path
from myqueue.task import task

# Double cores/time at most 4 times for out of memory/time tasks:
rtask = functools.partial(task, restart=4)


def create_tasks():
    folder = Path.cwd().name
    if folder not in ['nm', 'fm', 'afm']:
        return [rtask('c2db.relax@8:1d')]

    if Path('duplicate').is_file() or Path('stop').is_file():
        return []

    tasks = [rtask('c2db.duplicate@1:15m', deps='../c2db.relax'),
             rtask('c2db.gs@8:1h', deps='c2db.duplicate'),
             rtask('c2db.densk@8:3h', deps='c2db.gs'),
             rtask('c2db.stiffness@8:6h', deps='c2db.densk'),
             rtask('c2db.phonons@8:14h', deps='c2db.stiffness'),
             rtask('c2db.stop@1:15m', deps='c2db.phonons'),
             rtask('c2db.anisotropy@1:1h', deps='c2db.stop'),
             rtask('c2db.vacuumlevels@8:1h', deps='c2db.anisotropy'),
             rtask('c2db.bandstructure@8:3h', deps='c2db.anisotropy'),
             rtask('c2db.polarizability@24:10h', deps='c2db.stop',
                   diskspace=2),
             rtask('c2db.plasmafrequency@8:4h', deps='c2db.anisotropy'),
             rtask('c2db.hse@24:20h', deps='c2db.anisotropy',
                   diskspace=1),
             rtask('c2db.hseinterpol@8:10m', deps='c2db.hse'),
             rtask('c2db.gap@8:3h', deps='c2db.anisotropy'),
             rtask('c2db.deformation_potentials@8:6h', deps='c2db.gap'),
             rtask('c2db.em@8:10h', deps='c2db.gap'),
             rtask('c2db.embzcut@8:10h', deps='c2db.em'),
             rtask('c2db.pdos@8:1h', deps='c2db.gap'),
             rtask('c2db.emexciton@8:6h', deps='c2db.pdos'),
             rtask('c2db.fermi_surface@1:10m', deps='c2db.gap')]

    if folder == 'nm':
        tasks.append(rtask('c2db.gllbsc@8:3h', deps='c2db.densk'))

    if 0:
        import numpy as np
        pbe_gap = np.load('gap_soc.npz')['gap']
        if pbe_gap > 0.3:
            tasks.extend([rtask('c2db.gw@192:xeon24:2d', deps='c2db.gs'),
                          rtask('c2db.gwinterpol@8:10m', deps='c2db.gw')])

    return tasks
