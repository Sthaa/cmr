import os
import traceback
from multiprocessing import Pool
from pathlib import Path

import matplotlib
from ase.db import connect
from ase.db.summary import Summary
from ase.db.web import process_metadata

from c2db.collect import collect
from c2db import readinfo, chdir
from c2db.run_postprocessing import main as postprocess

matplotlib.use('Agg')

folders = """\
AuSe
BN
BiTeI
C2
C3N
CuI
FeSe
GaS-1T
GeS2
MPX3
MX2-1T
MX2-2H
MX3-1T
MX3-2H
M2C
M3C2
M4C3
MoSSe
P4
PbSe
PdSe2
Perovskite
ReS2
TMD
TMDH
TMD_alloys
TMHC
TMM
TiS3
WTe2
Xanes
chalcohalides_workflow
#** not collected **:
#MX2-2H-afm
#TMD-afm"""

folders = [f for f in folders.splitlines() if f[0] != '#']

home = Path('/home/niflheim2/cmr/C2DB/')
refs = '/home/niflheim2/cmr/OQMD12/oqmd12.db'


def collect_folder(folder):
    path = home / folder
    db = connect(path.name + '.db')
    err = open(path.name + '.err', 'w')
    for subfolder in path.glob('*-*/*m/'):
        print(subfolder)
        with chdir(subfolder):
            info = readinfo('..')
            # prototype = info.get('prototype')
            try:
                errors = collect(info, db, verbose=True,
                                 skip_forces=True,
                                 references=refs)
                for a, b, c in errors:
                    print(a, b, ''.join(c), file=err)
            except KeyboardInterrupt:
                break
            except Exception as x:
                tb = traceback.format_exc()
                err.write('{}: {}: {}\n'.format(subfolder, x, tb))
    err.close()


with Pool(processes=6) as pool:
    pool.map(collect_folder, folders)

txt = ''
for folder in folders:
    path = home / folder
    err = Path(path.name + '.err').read_text()
    if err:
        headline = str(path)
        txt += '{}\n{}\n\n{}\n\n'.format(headline, '=' * len(headline), err)

if txt:
    import smtplib
    from email.message import EmailMessage
    msg = EmailMessage()
    msg.set_content(txt)
    msg['Subject'] = 'C2DB collect errors!'
    msg['From'] = 'c2db@niflheim.dtu.dk'
    msg['To'] = ','.join(['jjmo@dtu.dk',
                          'sthaa@fysik.dtu.dk',
                          'mogje@fysik.dtu.dk',
                          'thygesen@fysik.dtu.dk'][:2])
    s = smtplib.SMTP('smtp.ait.dtu.dk')
    s.send_message(msg)
    s.quit()
    1 / 0

# Combine:
db2 = connect('c2db-big-1.db')
for f in folders:
    path = home / f
    db = connect(path.name + '.db')
    with db2:
        for row in db.select():
            db2.write(row, data=row.get('data'), **row.key_value_pairs)

postprocess('c2db-big-1.db')

# Create png's:
db = connect('c2db-big.db')
db.python = '/home/niflheim2/cmr/software/cmr/c2db/custom.py'
db.meta = process_metadata(db)
os.chdir('static')
for row in db.select():
    Summary(row, db.meta, prefix='c2db-{}-'.format(row.uid))

# Copy c2db.db and static.tar.gz to web-server
# Copy c2db.db and c2db-big.db to ~/cmr and do backup every second month
