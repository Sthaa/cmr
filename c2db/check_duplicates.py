# Creates: c2db_duplicates.db

from __future__ import print_function
import ase
import numpy as np
from c2db.distance_matrix import get_distance


def reduce_formula(count):
    """Reduce count to unique formula.

    Example::

        >>> reduce({'Ti': 2, 'O': 4})
        'O2Ti1'
    """
    N = count.values()
    m = min(N)
    for div in range(m, 0, -1):
        if all(n % div == 0 for n in N):
            break
    return ''.join('{}{}'.format(symbol, n // div)
                   for symbol, n in sorted(count.items()))


def find_duplicates(row, rows, threshold=0.05, use_magstate=False):
    distances, ids = distance_to_rows(row, rows, use_magstate)
    indices = distances <= threshold
    return list(ids[indices])


def distance_to_rows(row, rows, use_magstate=False, permute_atoms=False):
    # If we are given a row of a database, we should only compare with
    # structures that have the same stoichiometry, but a different id.
    if isinstance(row, ase.db.row.AtomsRow):
        atoms = row.toatoms()
        formula = reduce_formula(row.count_atoms())
        current_id = row.id
        candidates = [x for x in rows
                      if reduce_formula(x.count_atoms()) == formula]
        candidates = [x for x in candidates if x.id != current_id]
        if use_magstate:
            candidates = [x for x in candidates if x.magstate == row.magstate]
    # Otherwise, if we have an ASE atoms object, only the stoichiometry
    # matters.
    else:
        atoms = row
        count = {}
        symbols = atoms.get_chemical_symbols()
        for symbol in symbols:
            count[symbol] = count.get(symbol, 0) + 1
        formula = reduce_formula(count)
        candidates = [x for x in rows
                      if reduce_formula(x.count_atoms()) == formula]
    # If there are too many atoms, we cannot calculate the RMSD. Better to just
    # treat all rows as different and check by hand later:
    if len(atoms) >= 50:
        candidates = []
    if not candidates:
        return np.array([]), np.array([])
    distances = np.array([get_distance(atoms, candidate.toatoms(),
                                       permute_atoms=permute_atoms)
                          for candidate in candidates])
    order = distances.argsort()
    return distances[order], np.array([x.id for x in candidates])[order]


def delete_duplicates(db, duplicate_db=None, threshold=0.05):
    rows = [x for x in db.select()]
    count = 0
    duplicate_count = 0
    duplicate_list = []
    for row in rows:
        count += 1
        duplicates = find_duplicates(row, rows[count:], use_magstate=True,
                                     threshold=threshold)
        if duplicates:
            duplicate_count += 1
            duplicate_list.append([row.id] + duplicates)
    print('{} potential duplicates found'.format(duplicate_count))
    ids_to_delete = []
    duplicates_of = []
    for duplicate_ids in duplicate_list:

        # Duplicates are removed in the following order of preference:
        # - If one material has fewer atoms than the other,
        #   the simpler material is kept.
        # - If one material has more key value pairs than the other, the more
        #   complete material is kept.
        # - If one material has more data than the other, the more complete
        #   material is kept.
        # - Finally, as a tie-breaker, the material with the lowest id is kept.
        min_atom_count, min_id = np.inf, np.inf
        max_kvp_length, max_data_length = 0, 0
        for entry in duplicate_ids:
            row = [x for x in rows if x.id == entry][0]
            atom_count = len(row.toatoms())
            kvp_length = len(row.key_value_pairs)
            data_length = len(row.get('data', []))
            current_id = row.id
            # Python tuples are compared lexicographically: If the first two
            # items are different, return the result of that comparison,
            # otherwise move on to the next element. This is exactly what we
            # want.
            if ((atom_count, -kvp_length, -data_length, current_id) <
                (min_atom_count, -max_kvp_length, -max_data_length, min_id)):
                min_atom_count = atom_count
                max_kvp_length = kvp_length
                max_data_length = data_length
                min_id = current_id
        duplicate_ids.remove(min_id)
        duplicates_of += [min_id] * len(duplicate_ids)
        ids_to_delete += duplicate_ids
    if duplicate_db is not None:
        for row_id, uid in zip(ids_to_delete, duplicates_of):
            row = [x for x in rows if x.id == row_id][0]
            duplicate_db.write(row, duplicate_id=uid)
    ids_to_delete = [int(x) for x in list(set(ids_to_delete))]
    db.delete(ids_to_delete)


if __name__ == '__main__':
    import sys
    import os
    from ase.db import connect
    db_file = sys.argv[1]
    db = connect(db_file)
    path, ext = os.path.splitext(db_file)
    path = path + '_duplicates'
    duplicate_db = connect(path + ext)
    delete_duplicates(db, duplicate_db=duplicate_db)
