from pathlib import Path
import os
from itertools import product
import numpy as np

from ase.data import covalent_radii, atomic_numbers
from ase.io import Trajectory


def generate_structures_from_prototype(prototype, p_ab, threshold=0.1):
    numbers = list(set(prototype.numbers))
    substitutions = [find_substitutions(number, p_ab, threshold)
                     for number in numbers]
    for substitution in product(*substitutions):
        if len(set(substitution)) != len(numbers):
            continue
        yield apply_substitution(prototype, dict(zip(numbers, substitution)))


def find_substitutions(number, data, threshold):
    row = data[number]
    substitutions = np.where(row > threshold)[0]

    allowed_elements = [
        'H', 'He',
        'Li', 'Be', 'B', 'C', 'N', 'O', 'F', 'Ne',
        'Na', 'Mg', 'Al', 'Si', 'P', 'S', 'Cl', 'Ar',
        
        'K', 'Ca', 'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn',
        'Ga', 'Ge', 'As', 'Se', 'Br', 'Kr',
        
        'Rb', 'Sr', 'Y', 'Zr', 'Nb', 'Mo', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd',
        'In', 'Sn', 'Sb', 'Te', 'I', 'Xe',
        # 6
        'Cs', 'Ba', 'La', 'Hf', 'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg',
        'Tl', 'Pb', 'Bi', 'Rn']
    allowed_numbers = [atomic_numbers[e] for e in allowed_elements]
    substitutions = [s for s in substitutions if s in allowed_numbers]
    return substitutions


def apply_substitution(atoms, substitution):
    new_atoms = atoms.copy()
    new_numbers = [substitution[number] for number in atoms.numbers]

    # Scale in-plane lattice vectors by the harmonic mean of the covalent_radii
    sf = (np.product([covalent_radii[n] for n in new_numbers])
          / np.product([covalent_radii[n] for n in atoms.numbers]))
    sf = pow(sf, 1. / len(new_numbers))
    new_atoms.set_cell(np.diag([sf, sf, 1]) @ new_atoms.get_cell())

    # update the atomic numbers
    new_atoms.numbers = new_numbers
    return new_atoms


if __name__ == "__main__":
    from c2db.utils import get_reduced_formula
    from c2db.substitutions import p_ab
    from ase.db import connect
    fnames = list(Path('.').glob('seed.*'))
    try:
        assert len(fnames) == 1, fnames
        fname = fnames[0].name
    except AssertionError:
        import sys
        fname = sys.argv[1]
    seed_structures = Trajectory(fname)
    for seed_structure in seed_structures:
        formula = seed_structure.get_chemical_formula('metal')
        prototype_name = get_reduced_formula(formula)

        db = connect('start.db')
        for structure in generate_structures_from_prototype(seed_structure,
                                                            p_ab):
            folder = '{}-{}'.format(structure.get_chemical_formula('metal'),
                                    prototype_name)
            if os.path.isdir(folder):
                continue

            os.mkdir(folder)
            structure.write(os.path.join(folder, 'start.traj'))
            db.write(structure)
