import argparse

from ase.io import read
from ase.parallel import parprint


def calc(atoms, **parms):
    # run over the steps
    parprint("Initial relaxation...")
    from c2db.relax import relax
    lparms = {k: v for k, v in parms.items() if k == 'ecut' or k == 'vacuum'}
    atoms = relax(atoms, **lparms)

    # continue? hform < 1 eV
    from c2db.references import read_references, formation_energy
    hform = formation_energy(atoms, read_references()) / len(atoms)
    if hform > 1:
        parprint("NOT stable (hform)!")
        return

    parprint("Stability...")
    from c2db.stability import relax2x2, dyn_stab
    lparms = {k: v for k, v in parms.items() if k == 'ecut' or k == 'stdev'}
    relax2x2(**lparms)

    # continue? dyn stab < 10 meV, rmsd < 0.05 A
    ediff, ddiff = dyn_stab()
    if ediff > 10e-3 or ddiff > 0.05:
        parprint("NOT stable (dyn stable)!")
        return
    
    parprint("PBE electronic properties...")
    from c2db.bandstructure import bandstructure
    from c2db.ehmasses import runeh
    from c2db.fermi_surface import fermi_surface
    from c2db.pdos import run_pdos
    from c2db.anisotropy import anisotropy
    from c2db.strains import run_strains
    from c2db.polarizability import polarizability
    from c2db.gllbsc import gllbsc
    from c2db.hse import runhse

    scripts = [bandstructure,
               runeh,
               fermi_surface,
               run_strains,
               run_pdos,
               anisotropy,
               polarizability,
               gllbsc,
               runhse]

    names = ["Bandstructure",
             "Electron hole masses",
             "Fermi surface",
             "Strain calculations",
             "PDOS",
             "Anisotropy",
             "Polarizability",
             "GLLB",
             "HSE"]
    for script, name in zip(scripts, names):
        try:
            parprint("Starting {0} calculation.".format(name))
            script()
        except Exception as e:
            parprint("{0} failed!".format(name))
            parprint(e)

    # exclude time consuming steps
    # from c2db.bb import bb
    # from c2db.gw import gw_calc
    # from c2db.bse import bse
    # bb()
    # gw_calc()
    # bse()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', nargs='?', default='guess.xyz',
                        help='Filename of input structure.')
    parser.add_argument('-e', '--ecut', default=800, help='cutoff energy')
    parser.add_argument('-v', '--vacuum', default=7.5, help='cutoff energy')
    parser.add_argument('--skip-long', action='store_true')
    args = parser.parse_args()

    atoms = read(args.filename)
    parms = dict(ecut=float(args.ecut), vacuum=float(args.vacuum))
    if args.skip_long:
        parms['stdev'] = 0.0
    calc(atoms, **parms)
