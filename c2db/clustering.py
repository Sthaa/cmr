# Creates: {prototype}_dendrogram.pdf, {prototype}_3D.pdf, {prototype}_2D.pdf

from __future__ import print_function
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D  # noqa


from scipy.cluster.hierarchy import dendrogram, linkage, cophenet, fcluster
from scipy.spatial.distance import squareform
from sklearn.manifold import MDS


def go(distance_matrix_split, stoichiometry=None):
    cmap = cm.tab20b
    grey = '0.7'
    keys = distance_matrix_split.keys()
    for key in keys:
        if stoichiometry is not None and key != stoichiometry:
            continue
        data = distance_matrix_split[key][()]
        distance_matrix = data['distance_matrix']
        formulas = data['formulas']
        prototypes = data['prototypes']
        n = len(set(prototypes))
        n_occ = len(formulas)
        if n_occ < 5:
            print(key)
            continue
        labels = ['{0}-{1}'.format(formulas[i], prototypes[i])
                  for i in range(len(formulas))]
        labels = [''] * len(formulas)
        unique_prototypes = list(set(prototypes))
        mapping = {}
        i = 0
        for prototype in prototypes:
            if prototype in mapping:
                continue
            else:
                mapping[prototype] = i
                i += 1
        inv_mapping = dict(zip(mapping.values(), mapping.keys()))
        colors = np.array([cmap(x)
                           for x in np.linspace(0, 1,
                                                len(unique_prototypes))])
        prototype_numbers = [mapping[x] for x in prototypes]

        Z = linkage(squareform(distance_matrix), 'average')
        link_colors = [x for x in 'rgbymkc']
        link_colors += ['tab:blue', 'tab:orange', 'tab:green', 'tab:red',
                        'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray',
                        'tab:olive', 'tab:cyan']
        link_colors += ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',
                        '#9467bd', '#8c564b', '#e377c2', '#7f7f7f',
                        '#bcbd22', '#17becf']
        
        def link_color_func(k):
            n = len(prototypes)
            k = int(k)
            if k < n:
                return colors[prototype_numbers[[k]]]
            first = Z[k - n, 0]
            second = Z[k - n, 1]
            if first < n:
                first = link_colors[prototype_numbers[int(first)]]
            else:
                first = link_color_func(first)
            if second < n:
                second = link_colors[prototype_numbers[int(second)]]
            else:
                second = link_color_func(second)
            if first == second:
                return first
            else:
                return grey

        c, coph_dists = cophenet(Z, squareform(distance_matrix))
        print(key, c)
        classes = fcluster(Z, n, criterion='maxclust')
        print(formulas[np.where(classes == 1)[0]])
        print(formulas[np.where(classes == 2)[0]])
        figsize = 10
        fig = plt.figure(figsize=(figsize, figsize))

        # Compute and plot second dendrogram.
        ax2 = fig.add_axes([0.0, 0.80, 0.79, 0.2])
        A = dendrogram(Z,
                       distance_sort=True,
                       labels=labels,
                       # orientation='right',
                       link_color_func=link_color_func)
        ax2.set_xticks([])
        ax2.set_yticks([])

        legend_lines = []
        for i in range(len(mapping)):
            legend_lines.append(mlines.Line2D([], [],
                                              color=link_colors[i],
                                              marker='',
                                              label=inv_mapping[i]))
        legend_lines.append(mlines.Line2D([], [],
                                          color=grey,
                                          marker='',
                                          label='Mixture'))
        plt.legend(handles=legend_lines, ncol=4)

        # Plot distance matrix.
        axmatrix = fig.add_axes([0.0, 0.0, 0.79, 0.79])
        index = A['leaves']
        D = distance_matrix[index[::-1], :][:, index]
        im = axmatrix.matshow(np.sqrt(D), aspect='auto', origin='lower')
        axmatrix.set_xticks([])
        axmatrix.set_yticks([])
        
        axcolor = fig.add_axes([0.83, 0.0, 0.02, 0.79])
        plt.colorbar(im, cax=axcolor)
        plt.title(key)
        plt.savefig('{0}_dendrogram.pdf'.format(key), bbox_inches='tight')
        if len(distance_matrix) > 3:
            n_components = 3
        else:
            n_components = 2
        subspace = MDS(dissimilarity='precomputed',
                       n_components=n_components)
        subspace = subspace.fit(distance_matrix)
        coordinates = subspace.embedding_
        if False:
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.scatter(coordinates[:, 0],
                       coordinates[:, 1],
                       coordinates[:, 2],
                       c=classes,
                       cmap=cmap)
            plt.savefig('{0}_3D.pdf'.format(key), bbox_inches='tight')
        if False:
            fig2 = plt.figure()  # noqa
            ax = plt.subplot(111)
            ax.scatter(coordinates[:, 0],
                       coordinates[:, 1],
                       c=classes)
            plt.savefig('{0}_2D.pdf'.format(key), bbox_inches='tight')
    plt.show()


def distance_function(row):
    atoms = row.toatoms()
    pos = atoms.get_positions()
    return np.abs(pos[0, 2] - pos[1, 2])
    print(atoms.numbers)


if __name__ == "__main__":
    import sys
    split = np.load(sys.argv[1])
    try:
        stoichiometry = sys.argv[2]
    except IndexError:
        stoichiometry = None
    go(split, stoichiometry=stoichiometry)
