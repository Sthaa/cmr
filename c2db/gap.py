# Creates: gap.npz, gap_soc.npz

import os.path as op

import numpy as np

from ase.dft.bandgap import bandgap
from ase.parallel import paropen

from gpaw import GPAW
from gpaw.kpt_descriptor import to1bz

from c2db.utils import gpw2eigs


def socstr(soc):
    return '_soc' if soc else ''


def bg(gpw='densk.gpw'):
    """find bandgap, vbm, cbm, k_cbm, k_vbm in densk.gpw and save it to
    it to gap.npz and gap_soc.npz
    """
    if not op.isfile(gpw):
        return
    calc = GPAW(gpw, txt=None)
    ibzkpts = calc.get_ibz_k_points()
    for soc in [True, False]:
        e1e2g, skn1, skn2 = gap(soc=soc, direct=False)
        e1e2g_dir, skn1_dir, skn2_dir = gap(soc=soc, direct=True)
        k1, k2 = skn1[1], skn2[1]
        k1_dir, k2_dir = skn1_dir[1], skn2_dir[1]
        k1_c = ibzkpts[k1] if k1 is not None else None
        k2_c = ibzkpts[k2] if k2 is not None else None
        if k1_c is not None:
            k1_c = to1bz(k1_c[None], calc.wfs.gd.cell_cv)[0]
        if k2_c is not None:
            k2_c = to1bz(k2_c[None], calc.wfs.gd.cell_cv)[0]
        k1_dir_c = ibzkpts[k1_dir] if k1_dir is not None else None
        k2_dir_c = ibzkpts[k2_dir] if k2_dir is not None else None
        if k1_dir_c is not None:
            k1_dir_c = to1bz(k1_dir_c[None], calc.wfs.gd.cell_cv)[0]
        if k2_dir_c is not None:
            k2_dir_c = to1bz(k2_dir_c[None], calc.wfs.gd.cell_cv)[0]

        efermi = 333.0
        if soc:
            _, efermi = gpw2eigs(gpw, soc=True, optimal_spin_direction=True)
        else:
            efermi = calc.get_fermi_level()
        data = {'gap': e1e2g[2],
                'vbm': e1e2g[0],
                'cbm': e1e2g[1],
                'gap_dir': e1e2g_dir[2],
                'vbm_dir': e1e2g_dir[0],
                'cbm_dir': e1e2g_dir[1],
                'k1_c': k1_c,
                'k2_c': k2_c,
                'k1_dir_c': k1_dir_c,
                'k2_dir_c': k2_dir_c,
                'skn1': skn1,
                'skn1_dir': skn1_dir,
                'skn2': skn2,
                'skn2_dir': skn2_dir,
                'efermi': efermi}
        with paropen('gap{}.npz'.format(socstr(soc)), 'wb') as f:
            np.savez(f, **data)


def gap(soc, direct=False, gpw='densk.gpw'):
    """Extension of ase.dft.bandgap.bandgap to also work with spin orbit
    coupling and return vbm (e1) and cbm (e2)
    returns (e1, e2, gap), (s1, k1, n1), (s2, k2, n2)
    """
    calc = GPAW(gpw, txt=None)
    if soc:
        e_km, efermi = gpw2eigs(gpw, soc=True, optimal_spin_direction=True)
        gap, km1, km2 = bandgap(eigenvalues=e_km, efermi=efermi, direct=direct,
                                kpts=calc.get_ibz_k_points(), output=None)
        if km1[0] is not None:
            e1 = e_km[km1]
            e2 = e_km[km2]
        else:
            e1, e2 = None, None
        x = (e1, e2, gap), (0,) + tuple(km1), (0,) + tuple(km2)
    else:
        g, skn1, skn2 = bandgap(calc, direct=direct, output=None)
        if skn1[1] is not None:
            e1 = calc.get_eigenvalues(spin=skn1[0], kpt=skn1[1])[skn1[2]]
            e2 = calc.get_eigenvalues(spin=skn2[0], kpt=skn2[1])[skn2[2]]
        else:
            e1, e2 = None, None
        x = ((e1, e2, g), skn1, skn2)
    return x


if __name__ == '__main__':
    bg()
