import os
import shutil


def bb():
    path = os.path.basename(os.getcwd())
    formula, prototype = path.split('-')
    phase = {'MoS2': 'H', 'CdI2': 'T'}[prototype]
    name = '../BB/{}-{}_'.format(phase, formula)
    for x in ['absorption_in_plane.csv',
              'absorption_out_of_plane.csv',
              'polarizability_in_plane.csv',
              'polarizability_out_of_plane.csv']:
        fn = name + x
        if not os.path.isfile(fn):
            return
        shutil.copy(fn, x.replace('_', '-'))


if __name__ == '__main__':
    from c2db import run
    run(bb)
