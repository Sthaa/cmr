"""Wallpaper groups"""

import numpy as np
from math import sqrt
from ase import Atoms
from ase.geometry import crystal_structure_from_cell as csfc
from gpaw.symmetry import Symmetry


# Input: Structure
def wallpaper(pos_t=None, cell=None, elements_t=None):
    '''Calculates wallpaper group based on atom positions, unit cell,
    and atomic elements'''

    # Collapse all atoms into same plane and remove duplicate atoms
    atol = 1e-1
    rtol = 0
    change = [0]
    log = []

    structure_t = Atoms(elements_t, positions=pos_t, cell=cell)
    structure_t.wrap(center=(0, 0, 0.5))
    pos_t = structure_t.positions

    pos_t.setflags(write=1)
    for i in range(len(pos_t)):
        pos_t[i, 2] = cell[2, 2] / 2

    for j in range(len(pos_t[:, 0])):
        for k in range(j, len(pos_t[:, 0])):
            if np.allclose(pos_t[j, 0:2], pos_t[k, 0:2], rtol, atol):
                if j != k:
                    change[0] = 1
                    log = np.append(log, j)

    if change[0] == 0:
        pos = pos_t
        elements = elements_t
    else:
        pos = np.delete(pos_t, log, 0)
        elements = np.delete(elements_t, log, None)

    structure = Atoms(elements, cell=cell, positions=pos)
    structure.pbc = (1, 1, 0)

    # Get crystal structure from cell
    cell = structure.get_cell()
    crystal_structure = {
        'hexagonal': '3',
        'orthorhombic': '2',
        'tetragonal': '1',
        'cubic': '1',
        'rhombohedral type 1': '3',
        'rhombohedral type 2': '3',
        'monoclinic': '4'
    }

    def get_crystal_structure(cell):
        return crystal_structure[csfc(cell)]

    # Get all symmetry operations (in lattice basis) together
    # with the lattice vectors
    try:
        id_a = structure.get_atomic_numbers()

        sym = Symmetry(
            id_a,
            structure.cell,
            symmorphic=False,
            time_reversal=False,
            tolerance=1e-2)
        sym.analyze(structure.get_scaled_positions())
        # atoms2symmetry(structure, tolerance=1e-2)
    except AssertionError:
        print('Error: cell is distorted: p1')

    rotaa = sym.op_scc
    trlaa = sym.ft_sc
    refl = np.zeros(1)
    glre = np.zeros(1)
    char = np.zeros(17)
    celltype = get_crystal_structure(cell)

    # Check if all atoms are identical. If there is one mismatch set
    # identical[0]=0
    L = len(structure.get_chemical_symbols())
    # Assume initially all atoms are identical
    identical = np.ones(1)

    for i in range(L):
        if not (structure.get_chemical_symbols()[0] ==
                structure.get_chemical_symbols()[i]):
            identical[0] = 0

    # Define rotation and reflection matrices
    rot60 = np.array([[1 / 2, -sqrt(3) / 2, 0], [sqrt(3) / 2, 1 / 2, 0],
                      [0, 0, 1]])
    rot90 = np.array([[0, -1, 0], [1, 0, 0], [0, 0, 1]])
    rot120 = np.array([[-1 / 2, -sqrt(3) / 2, 0], [sqrt(3) / 2, -1 / 2, 0],
                       [0, 0, 1]])
    rot180 = np.array([[-1, 0, 0], [0, -1, 0], [0, 0, 1]])

    ref0 = np.array([[1, 0, 0], [0, -1, 0], [0, 0, 1]])
    ref30 = np.array([[1 / 2, sqrt(3) / 2, 0], [sqrt(3) / 2, -1 / 2, 0],
                      [0, 0, 1]])
    ref45 = np.array([[0, 1, 0], [1, 0, 0], [0, 0, 1]])
    ref60 = np.array([[-1 / 2, sqrt(3) / 2, 0], [sqrt(3) / 2, 1 / 2, 0],
                      [0, 0, 1]])
    ref90 = np.array([[-1, 0, 0], [0, 1, 0], [0, 0, 1]])
    ref120 = np.array([[-1 / 2, -sqrt(3) / 2, 0], [-sqrt(3) / 2, 1 / 2, 0],
                       [0, 0, 1]])
    ref135 = np.array([[0, -1, 0], [-1, 0, 0], [0, 0, 1]])
    ref150 = np.array([[1 / 2, -sqrt(3) / 2, 0], [-sqrt(3) / 2, -1 / 2, 0],
                       [0, 0, 1]])

    # Has reflection?
    for i in range(len(rotaa)):
        # Write symmetry operations in (x,y,z) coordinates
        rotxy = np.dot(np.linalg.inv(cell), np.dot(rotaa[i], cell))
        trlxy = np.dot(np.linalg.inv(cell), trlaa[i])
        if (np.allclose(rotxy, ref0, rtol=rtol, atol=atol) and
                np.allclose(trlxy, [0, 0, 0], rtol=rtol, atol=atol)):
            refl[0] = 1
        elif (np.allclose(rotxy, ref30, rtol=rtol, atol=atol) and
              np.allclose(trlxy, [0, 0, 0], rtol=rtol, atol=atol)):
            refl[0] = 1
        elif (np.allclose(rotxy, ref45, rtol=rtol, atol=atol) and
              np.allclose(trlxy, [0, 0, 0], rtol=rtol, atol=atol)):
            refl[0] = 1
        elif (np.allclose(rotxy, ref60, rtol=rtol, atol=atol) and
              np.allclose(trlxy, [0, 0, 0], rtol=rtol, atol=atol)):
            refl[0] = 1
        elif (np.allclose(rotxy, ref90, rtol=rtol, atol=atol) and
              np.allclose(trlxy, [0, 0, 0], rtol=rtol, atol=atol)):
            refl[0] = 1
        elif (np.allclose(rotxy, ref120, rtol=rtol, atol=atol) and
              np.allclose(trlxy, [0, 0, 0], rtol=rtol, atol=atol)):
            refl[0] = 1
        elif (np.allclose(rotxy, ref135, rtol=rtol, atol=atol) and
              np.allclose(trlxy, [0, 0, 0], rtol=rtol, atol=atol)):
            refl[0] = 1
        elif (np.allclose(rotxy, ref150, rtol=rtol, atol=atol) and
              np.allclose(trlxy, [0, 0, 0], rtol=rtol, atol=atol)):
            refl[0] = 1

    # Has glide reflection?
    for i in range(len(rotaa)):
        # Write symmetry operations in (x,y,z) coordinates
        rotxy = np.dot(np.linalg.inv(cell), np.dot(rotaa[i], cell))
        trlxy = np.dot(np.linalg.inv(cell), trlaa[i])
        if (np.allclose(rotxy, ref0, rtol=rtol, atol=atol) and np.allclose(
                np.dot(rotxy, trlxy), trlxy, rtol=rtol, atol=atol)):
            glre[0] = 1
        elif (np.allclose(rotxy, ref30, rtol=rtol, atol=atol) and np.allclose(
                np.dot(rotxy, trlxy), trlxy, rtol=rtol, atol=atol)):
            glre[0] = 1
        elif (np.allclose(rotxy, ref45, rtol=rtol, atol=atol) and np.allclose(
                np.dot(rotxy, trlxy), trlxy, rtol=rtol, atol=atol)):
            glre[0] = 1
        elif (np.allclose(rotxy, ref60, rtol=rtol, atol=atol) and np.allclose(
                np.dot(rotxy, trlxy), trlxy, rtol=rtol, atol=atol)):
            glre[0] = 1
        elif (np.allclose(rotxy, ref90, rtol=rtol, atol=atol) and np.allclose(
                np.dot(rotxy, trlxy), trlxy, rtol=rtol, atol=atol)):
            glre[0] = 1
        elif (np.allclose(rotxy, ref120, rtol=rtol, atol=atol) and np.allclose(
                np.dot(rotxy, trlxy), trlxy, rtol=rtol, atol=atol)):
            glre[0] = 1
        elif (np.allclose(rotxy, ref135, rtol=rtol, atol=atol) and np.allclose(
                np.dot(rotxy, trlxy), trlxy, rtol=rtol, atol=atol)):
            glre[0] = 1
        elif (np.allclose(rotxy, ref150, rtol=rtol, atol=atol) and np.allclose(
                np.dot(rotxy, trlxy), trlxy, rtol=rtol, atol=atol)):
            glre[0] = 1

    # Find smallest rotation
    for i in range(len(rotaa)):
        # Write symmetry operations in (x,y,z) coordinates
        rotxy = np.dot(np.linalg.inv(cell), np.dot(rotaa[i], cell))
        trlxy = np.dot(np.linalg.inv(cell), trlaa[i])
        if (np.allclose(rotxy, rot60, rtol=rtol, atol=atol) and
                np.allclose(trlxy, [0, 0, 0], rtol=rtol, atol=atol)):
            if refl[0] == 1:
                char[0] = 1  # p6m
            else:
                char[1] = 1  # p6
        elif (np.allclose(rotxy, rot90, rtol=rtol, atol=atol) and
              np.allclose(trlxy, [0, 0, 0], rtol=rtol, atol=atol)):
            if refl[0] == 1:
                for k in range(len(rotaa)):
                    if (np.allclose(
                            np.dot(
                                np.linalg.inv(cell), np.dot(rotaa[k], cell)),
                            ref45,
                            rtol=rtol,
                            atol=atol) and np.allclose(
                                np.dot(np.linalg.inv(cell), trlaa[k]),
                                [0, 0, 0],
                                rtol=rtol,
                                atol=atol)):
                        char[2] = 1  # p4m
                    else:
                        char[3] = 1  # p4g
            else:
                char[4] = 1  # p4
        elif (np.allclose(rotxy, rot120, rtol=rtol, atol=atol) and
              np.allclose(trlxy, [0, 0, 0], rtol=rtol, atol=atol)):
            if refl[0] == 1:
                for k in range(len(rotaa)):
                    if (np.allclose(
                            np.dot(
                                np.linalg.inv(cell), np.dot(rotaa[k], cell)),
                            ref30,
                            rtol=rtol,
                            atol=atol) and np.allclose(
                                trlxy, [0, 0, 0], rtol=rtol, atol=atol)):
                        char[6] = 1  # p3m1
                    elif (np.allclose(
                            np.dot(
                                np.linalg.inv(cell), np.dot(rotaa[k], cell)),
                            ref60,
                            rtol=rtol,
                            atol=atol) and np.allclose(
                                trlxy, [0, 0, 0], rtol=rtol, atol=atol)):
                        char[5] = 1  # p31m
    #                else:
    #                    print('Uidentified wallpaper group')
            else:
                char[7] = 1  # p3
        elif (np.allclose(rotxy, rot180, rtol=rtol, atol=atol) and
              np.allclose(trlxy, [0, 0, 0], rtol=rtol, atol=atol)):
            if refl[0] == 1:
                for k in range(len(rotaa)):
                    for l in range(len(rotaa)):
                        # Check for perpendicular reflection
                        if (np.allclose(
                                np.dot(
                                    np.dot(
                                        np.linalg.inv(cell),
                                        np.dot(rotaa[k], cell)),
                                    np.dot(
                                        np.linalg.inv(cell),
                                        np.dot(rotaa[l], cell))),
                                rot180,
                                rtol=rtol,
                                atol=atol) and np.allclose(
                                    trlxy, [0, 0, 0], rtol=rtol, atol=atol)):
                            # Distinguish with rectangle/rhombus
                            if celltype == 1:
                                # Check if all atoms are identical
                                if identical[0] == 1:
                                    char[9] = 1  # pmm
                                else:
                                    char[8] = 1  # cmm
                            elif celltype == 2:
                                char[8] = 1  # cmm
    #                       else:
    #                            print('Uidentified wallpaper group')
                        else:
                            char[10] = 1  # pmg
            else:
                # Glide reflection
                if glre[0] == 1:
                    char[11] = 1  # pgg
                else:
                    char[12] = 1  # p2
        else:
            if refl[0] == 1:
                # Distinguish with rectangle/rhombus
                if celltype == 1:
                    char[14] = 1  # pm
                elif celltype == 2:
                    char[13] = 1  # cm
    #            else:
    #                print('Uidentified wallpaper group')
            else:
                # Glide reflection
                if glre[0] == 1:
                    char[15] = 1  # pg
                else:
                    char[16] = 1  # p1

    wpg = None

    if char[0] == 1:
        wpg = 'p6m'
    elif char[1] == 1:
        wpg = 'p6'
    elif char[2] == 1:
        wpg = 'p4m'
    elif char[3] == 1:
        wpg = 'p4g'
    elif char[4] == 1:
        wpg = 'p4'
    elif char[5] == 1:
        wpg = 'p31m'
    elif char[6] == 1:
        wpg = 'p3m1'
    elif char[7] == 1:
        wpg = 'p3'
    elif char[8] == 1:
        wpg = 'cmm'
    elif char[9] == 1:
        wpg = 'pmm'
    elif char[10] == 1:
        wpg = 'pmg'
    elif char[11] == 1:
        wpg = 'pgg'
    elif char[12] == 1:
        wpg = 'p2'
    elif char[13] == 1:
        wpg = 'cm'
    elif char[14] == 1:
        wpg = 'pm'
    elif char[15] == 1:
        wpg = 'pg'
    elif char[16] == 1:
        wpg = 'p1'

    return wpg
